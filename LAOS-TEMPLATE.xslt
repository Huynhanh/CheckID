<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl js" 
                xmlns="http://www.w3.org/1999/xhtml" xmlns:js="urn:custom-javascript" 
                xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <style>
                    @page{
					size: A4;
                    margin: 0;					
                    }
					*{
                    margin: 0;
                    padding: 0;
                    }
            
                    p{
                    margin: 0px;
                    padding:0px;
                    }
            
                    html,body {
						color: #000;
						font-size: 10pt;
						font-family: "Open Sans";
						margin: 0 auto;
						line-height: 20pt;
                    }
                    
				   .wrapper{
                    margin: 0 auto;
                    width: 21cm;
                    height: 31cm;
					
                    }
					.wrapper-inner{
						background-color: #92d050;border-radius: 15px;padding: 0 10px 15px 10px;margin: 15px 10px 0 10px;
					}

                    .header-page-one{
                        padding: 25px 30px 30px 30px;
                        text-align: center;
                        position: relative;
                    }

                    .header-page-one .title{
                        display: flex;
                        flex-direction: column;
                        justify-content: space-around;
                        align-items: center;
                    }

                    .logo-left{
                        position: absolute;
                        left: 30px;
                        top: 5px;
                    }
                    
                    .logo-right{
                        position: absolute;
                        right: 30px;
                        top: 10px;
                    }
                    .general-info{
                        padding: 5px 0 0 40px;
                    }

                    .first-name, .last-name, .DOB, .age{
                        padding-right: 20px;
                    }
                    .first-name, .last-name, .vaccination-id, .DOB, .age, .address{
                        font-size: 14px;
                    }

                    .lable{
                        padding-left: 15px;
                    }
                    .table-one{
                        padding: 10px 40px 0 40px;
                    }

                    /*table{
                        width: 100%;
                        border-collapse: collapse;
                        text-align:center;
                    }

                    table, th, td {
                        border: 4px solid black;
                    }
                    
                    th, td{
                        border:1px solid black;
                    }*/

                    .table-two{
                        padding: 10px 75px 0 75px;
                    }

                    .header-page-two{
                        padding: 30px 0 10px 0;
                        text-align: center;
                        position: relative;
                    }

                    .header-page-two .title{
                        display: flex;
                        flex-direction: column;
                        justify-content: space-around;
                        align-items: center;
                    }
                    .footer{
                        display: flex;
                        justify-content: space-between;
                        padding: 35px 75px 0 55px;
                    }

                    .detail{
                        width: 63%;
                        line-height: 21px
                    }
					
					.table1  {
					   border: 3px solid black;
					   border-collapse: collapse;
					}
					.table1 th {
					   border: 1px solid black;
					padding: 5px 0 5px 0;
					}
					.table1 td {
					  border: 1px solid black;
					border-collapse: collapse;
					padding: 5px 2px 5px 2px;
					}

                </style>
            </head>
            <body>
                <div class="page">
                    <div class="wrapper">
						<div class="wrapper-inner">
							<table style="width:100%; padding-top: 15px;">
								<tr>
									<td style="width: 150px; padding-left: 10px;">
										<img alt="Logo" src="data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAIvJJREFUeNrse2eUXcWZ7a6qE28OfTurk9TdyhFEFiILDAgEJpjwcASbZBuPjfE4jO0Zgwk2YBswDDYYAzbBRggQiCAZRRRbEXWrc7c63b753pOr5kcLhRmYZT+/mbfWm3fWqj+3+55b+9T37W9/u+oQIQT+J10U/8Ou/3GAJavjwCf/0e/HgYceRnHXOhB/FM64hbJgEo7FwJVyuIxh0iXnoW/NKiiJGGLzzsDoe8+ABjLM+ZOjJ77Ai65eEsbKKWi+8FxYmzZdI/3p5dMl8Klu0Qq6AJF9WpYwuq/Y3Lo72N3zp3+lbPAWSUK5wqBRHWySAmmKH/xDC9XlFdi7ey9OP28xCqk8tnfvBBsVWHL+xdi+fSvmzpqFkfERrF+/DpqmfTymv/eJEUIhiAyRtaq5MdjsF73/EDwlGzJAY/pU9f3SMClY6dTDI3d9b0DL576dUWMzRqUA4ie3YNKcKdi8YiPU5Niiij1dCIT1UcMy/2iViuWiKnat0MmwcD0VAm8QTobxf4Bu/g7ABMJl8Cwvrtl9Xw9PHbpGj7xa47vMkeSEAJMlUIrplJd9oPqkx//1hOv4u6Kmo42XzUgrPjx11yloWlSHOwLvYsPmQUxFqtBkpTunzQvie9Hx1B/aVlyk3lC9mA3bsPfkhxzNelpQ/ByuGP7vBywA4QByPPUVL3fft2uuH6kVBPDyEogtIfe6DFgavAAbc3YFfjHoY+3r0mEoKHReke1AU/4gmu98FoW6IL78wSjOplHsCtfniro+dtkFJ4HGaeXTa55qrD5+HorCQ/Ev26oCi8PfSvrda0kn/SMV9H4KOvjfA5gDgkhRLdr1UOL6/mtAHeJ0KYBKAFeBMcTBNACaBVoR2sbiX3x9msHxi64BBHa1jVdseMuFRLsFryrxAcavTY3ppGgkMrF4+uDchWO7n+7C6rqqgfpZrW/3/7Tt81Jch29hBTAjBOnN/prW82Jf2xXsu87bnPyhzNjDAi5A/itYmgDwCLjDykNzx/9cdUfXtdwAsbs1kBjgDstwRwSUBhWsSYAESSqzsuYOKVqbCrTOxeR//CYi55/3zKgszdtbP3X+2GOPzjXeXD6/7cxPzRsNxuaZDZNuqL35s0authyvv/IKRjePvc8GCgg2BKGcUwkpqsCK+GCeOglW3C3zLg4+JE8JfBW2wN+S2381YO5QkJB9nN46tjZ8Uf+i4kYddp8MKSrB3qWBANAWGDA7ZVgf6ijmjv9x+Mq79nJZhQcXMCwI0+3nDt3ta3QKQv2Zj9W+qVZfU20SIfd7Jb7FswiK2RIG+vuQ7BharlWVvemVqcj9vgfW9hTUmWHYCQ28x0T48kbEvjX9bhGhvwFHkFIK8Vcgp38FNYELBp9iLq09bf8boQtHmnOrfCABQKnmMPcw0KgNN8Ng9kvwHTcMzzLWZnc1PEVkCURmkFsq4YQV8NNDCD7nouInJ8PHG89i2RVXx0/vhfIPNvz3JRA64T0k5qQxpWk2pkxrTk/LN12NzcU9AZ8A9TPwrAt7dxoKY/Da87B682rwupobPiBtP+n5sEOTJfnvz2FOGFgxv7Ty+gNPMC1TZh1Qoc9xUVwTgKS7kJpMeEkCudaFWm8jv61uj9Zy1+V1iyel1IpaiAAFicnwCAFiTJZqJUFCH7p8t1jMZhYX2eu13/JQgUrThGRJPfa55wThr/kccqUoFEbT9z509/PeBeRHTk8RfNyB/8JaiJYQrH0ZGKuGkfiXebCSxs2l18zZqk+7RGIs9Z+tNPvuHd8AYexjB/X5UfzLxkWR+W88roasKrfAwFQBu12GyACsXgLRKbyCB326h+xq3yrfid+9NHbRZ0e0ikpIPhXC50F4HITJ8JKd5aJ9xb/I/r7bYI9fIHKpGj40fBqrlc6ldUv3EHVuMhAsQyQsoW8kCkVl0DX9L90HR+e4MTFNbQ3DSpVA8i6IwqBOiyK/fgQwPQTPq6oPloen9b6675W8VXAZYx+/wkMP3PNJigJuHn5f0+p7g/PsOnfQB2F6EAECqsjQFphwOhnUchPKDI70xtgrme3ss8Ez1TRMB8I0PiL1Q5lD4ZpkmA4e/6gojf0r9I644IC0MNTK6+M/k31L9oHNBYMNn1REPNoFxiguu+TTwLP89lf7Vwu2QFtmPjsINIUhVehg03zgm0bggCN8RRM2v7fz4mJ87F6frd8Cl38se0uZD3Z8QnarCDalb/ZN7VtY2BSAf74NmVIUNyhgFRbU6SbclAwXpFQ8kHh87N3Ij9VIOj0BUUzQOrUg6cVD97OhN9RCmvXUbsLvXe2taF9IKymcLdFNWt0P3mSoBzwBUAYKFaASuOegWCpCC/v6S6+Pfz23Z+z42BenTKKShMwr3QiTGgRPrwJdEIazLYsp2QpUXj/v5vd+sryNEfo4of8RseSbf/zHrC6F0T7QGDz5w1sBwBuT4WZcwGaQyjy4IxqMPQSBc7MYeXjac9Z4/Bs0mHSPfqKEBOGaj8IrvAhCAgAECFPg5WgZ39d5nMMv/4wya2hMCnb/wD2Qq3fV1b0QLgBAJx7K1UoMlSJQCQF3PJjjpd6KsoqlvoPy74aTQzNCi6vBGoLIvzmAL5VdhJbYJNQ0VWHeggW4flv2rteff31Nc2Nzu+M4xwKOXnTBxzCVH/7Gh26Q4+na0oYwwhfnIVwCs18AyQDUBUVQH8fBh6c+6Raqv0CVEmAfHR0KuNeH4p49EG4VLEWGSyYiTFBu+wunfV7Y80ayP3vMTXz1litzfR2c5/MAIYcqg4eYvw5j1kIILoELDls4iOux7eUbg5/u83U+o13ZPD/5wQDqSzFcPXkptrRtRne2C1wGzjhlccP29du+Z9v2tcFg6JhyJYUnNx0byZoPyeV/nExyb38NkCG3OLB7VBhbNfjOzMLzbNCgjfRz5c8XBsM3KawIyAa4WQKXioDgAAPuW/sSuvoLCAUqcVrbCOotBocCXPBcOBDNBYxVD1ntxuQDP8x9qun714AIAIfMCEIIAq6DnlweVGLQNA3hcBi6X0egPLSPvWPdMParfWsq6iqin1t2A9r79uOV1/8EiTJ8d+EPseCqBXip780L9z6z9TPHTz/xWe8QkwAASb30h2OjWfEhv/p3P4le+PKdhVcroUw1oLRYcNIUdruMwEITuR31W53U0jM8meYppaDUgWUUoUg2ai6+Bt/r2Y373n8esWAcsVAMt76wDyfkJJQUBhMU8YRxRfXcPb9nAbDhthNub3nizw/LIR1wxaEwmJiLY5dQcl2UbBOu44BSCkIJGKf4/D/f+GUz4P7spbt+rw709iJaFkdFogJPvPgkduQ+xAbeBr46vT3Q758vyJEVJm3T5x8jM7jpVVTc2LNar8FUt8RAfIC9l8J3ogWrT4LIusglbzi/+o67VsI1JuYmCBilgMfx3fefwyNtKxHW/QhqGoKyH5VCx3cqg/DveAv1JbnCq828TZvHZgIcUne0nyTP+UxHgK19uSKEDUwgoeqQJQmGY+PGEy/F4qZ5KLrW4Qjw+XxY9ullWLd13SMzrltw09SaVtxz2feRKmXw1RXfwYZVa9HwhXlwDYfzHwycG4+XveNxbyKk9en+o7ogAkLsZWqMTeVpCQIeWMiDkgBISQJRgNyamhVetPBW8snHAY9/1DwhQhWsI3n8Ir8BMdV3hL84R6HChyeq67Gvow5TDXNk8QfKN6+Yll5B8yZ9pLPyBx+gd90WWUU2NQpuOoj6glBkGa7n4tsrH8VPl3wZcxKTYXoTROF5HoQloHnqbclUumJDZPulVzx0A1KhEvKlAibfcQI49yAO5CmtV88vHMy/A5kccjziLUe4ylNQ1rj1NKXWQP79IPTpHgjjkI5zYG3W4BWDWTHrwm8xpnNrPH8YbIDJ2Fsawj9KHQiFdRwVQRAAFA8IO0CfQmOb6vTPt0eUc6+oJ8TqtfF8VfCHG6OhK+MFZ2OMKU/YMu3XZQWKJANMhuO5+OYbj2BuOoKYycDpBLfVN9ahaXKj07mv4+69YuDczI2V/sLKcaDPBPVJsNoL0E4rR3Jr6txZolHTYj5TCAGJZ9OHhQbPOw3ScQOLrUEFaqsFIRjsfh1SyQS3HJi5GQ8oiy7eSxRyOLxkwtBbGsEt+19CT0GC/6jmhYBAZjIKZjE0WBi/sqB639FlqX6oWIJpC3iQkTaMGuJTatyAem7Gs2+STP5lAvLyRw9LogwuOLZGxtGyT0C1CQQFKKWQZQmQ2Ae03XvQWD96l5wH1POr4aYtEInAHSpBLdNb9mz7cIHSzdYJIiApSy6dWAlZBdu/7QStdlWV3SmDcwVKA4fiL4A4FKJWzZnt5gpp59M4uh9TGMOvrCT2WTnEqXwMWNu15w9747cNpbOn7TIONnmeByOdGw5z3xoi5MviXs4+2Vf2UJfh+Y1C9io37CtXA9IzrvDOVSCt/eheDARQGD4kI3C29gLSkZ6HEgrVJb/FoHOjb0lVHJ4AGAGVKYhMUfYP09Vkauc/ez3mMipLKQlbJ5SW8BRoFXtbiCJgD+hQZtgobSyD/5RxuMOAk5u8L3T6F7YRL3/4x3Qqoc0cwav9BxCQNdiH4EqCgAvR4nml5WFVqsnWlqMwNAbJEc/ODTd+454ZF5d85M5L4CWdS1336eW6ui/A5CdHMtlXrJi/LuMZd5VZ/gv40bnBKVAZAwLjEOzYJo9YosOwlU1Sk/+C9C87oM+PQW0Nw+ktILvuIPQLEgtu1a+NT442pKSAuuYQYApJTreIkgIpwUE4h1SVRHF3DLIyDkb0kcicOkCYh39IU2VkHtmJz221oSoCBIBNgVeqOJJN6n1kUnlNxnDglkwklOADBtw7Gv0xnKUilM2VhA8qPaO3U7olEsRv66buCLv2G+mSeWNJYy17x/siIMgco4eEwLxTJqFGaHCPijLmUfT5x7szW5PQZ4QBw4P9YQ6i6MI9WAQJyIFH1j42zz1gdEju8MQXuQXIEQS9rAuqSmBhBhpyIDkGWJgi9XxvKvP2tSDSxP8zIpB0ZfQNl2Ey10AIP6RKKU43nPmvRcVCS87knKz59uXzz3hyfe+u1xm38CljLca3vSjYHNvmHqhl66CT5yPMAKuUiQkh4IHTkjDov1f/XKJoy3ajM10Ap/QYZmQpWgxWV8F3YhmInyH9VCeCS2shggxSuYb8nt6FmYODbZIjtImUlKEqMatJpPyQdAGng4L4GVASoNMdUE8fk4QKcpisBAxTh2FRSKSEjyZHKC0/qcga1iRLM9LENqO+YOnU1nnina42tFIb9WoIUppyqThm6QVDWy1Fnf4pMzC67Z2vjVnZZVosBM1jg7VqeUqQY7s34nBYnSk4OQvkaMCeAOq0gJc0IAiBkzIRuKgGPONArfaBMobCmHWRrGg7pPEOGRAA9VCunUjiWgwY0SUkqAM5IGClFYRDBK+l81U/WT8CXSOHLa6cZeHXTz2F5uZmGIbx0cSSuqy+7st0mNf/4cewXBumY0N1ObYYLm5TqzHPyorvGSkzGQiFv5Vg3+nd9c7MvJWapYYCTJOUUtSUvqfI7Ng2XqLgJRN02IYmByYWVgjA5YDJwZr92dAVDTB7cqCaBGVOFMUVA2AxBe3rxtAwZ3bm5qsv7JASARcQgE/xQut0KcYG4ujhKq6cPQBZcVEKCzy9T8L3LX7JaGPwOAa+5aOSZBrAa91tuPuypbAsG1xwAOC6opmNnS5cz8XcSa3QJRWEAxoAIdzWNxT3Kzt6qxNdsqR4VfK1EnEQKU9Ap0pXzFVvotx+T9BjV5cSgtzufhR4EcSlEEJA9etw5kThySRm2tZSuy2HYKYIaW4cyDpQYyoG3jiIdDQANTfa/fL2t5Is1lqLXZLAVs7zLwl+Wc1Mt+KdoozXxsPohA+JKRau/EUQyaCq3HLp1ZsnkdC2qmAUjWVVmFI5CV2796OUy2P+3DlwLAvcdUE9jmQxi3h5Ba468VNIF3JYt3+bUoB9Z1H1nnJ8bNFBFpC4UMAYcXVH7JcNcX8I2o0hPbDHNgxQQj+qOyAyQ/bdD1Ha3ANqCZCiC+QtMNvDcTVT4Y8ElO4GXJ/pKVXKrkCkJQB33MbAhjTSgxZQ5Ye5c2Bb5/s73pV+II0curHgbDjgqHsCaGsvYaixCt3pMWRIAKJChawAK99/b8lkN7wmoPk73EPaVAlH8edXXkG8rAxXXnUlSvk8bO4CmoIzpi4EIxQyYyRLrUd5zP9Zx3aAjDV4SaKr/PyimfnLvporljeFt1CIAgggBD82bxlF9u19KG7vA9XkY3p2yglKhcJkYYqz5YiocgiQmxSFN2iip72IJFPB/CW4OkNC6JUVUlWlpKjqR5wP5pPI290CRNWh5Eo4kJXQYURBkQMv2dinJC8bGx0tr07KZzNdOdwBO46Dxx57DM2zpmJqw2S8uf8DPLn9DQRVHRJlGC2kLzF8+KwwLSgWebzBUr//67KhN8tsI7LsXWlrlSUKLzcEIXSAUAJCCCZsV0yA3do7AZYf1fW4HFZr8GvrfAd/LGpCPnAZkumiNG6gq8dCKqCB5ksgYQ1ywQVcLizXKpPowUONt8eJsDzCYj6Qch/EUAFeWAU8F8S0QEMqNC6QDhRPa6Lx2bfeePsWy7UP969wOX767nNYNb4TrusibxYnPgcgIC4LVZVB5M2kosV+dMNY51DcplLBkyi3JXbNvgKKhOG1Jh9MYcJ2PXDGYO4fQm7/IGi5D97RBOYJiLD/4uxZlfcLIghcDkIJvEo/WMpCJq6D7hkFrw2DgID2ZeHsHu4aS1k9Utk4AecclMMyXWdvoVmbS4rOkaeZMUBlBqrLEJSClmnIDbI7kyMjl3v0yBOXCUO9G4LLPShgSOjhIxKTOyMu5xASLfNK+bP/oMde+wwZImHHws9OqCXPz9YxpBJEcg6ylg1CCCzHhagMIt5QDSlnQ7AjNYq6AoMLI7d7MogvI7pYXz6dP6VyAUubgERB0gaE5QEyBcZKCPYY3XEt/CM34Q1JkklQyBuQGGuyW8NTwQVEyoCQKKjCIEwK7gnA4yCOgGLD7g4XL/3Rr3/6pWCJ/pp8JPO4AFElVC1pBZkUhrC9w4Bd4T06lM9c6QXkGi6JJ/czfbDf02LZMps93qr8vqiJF4Mmf5YLmFyIw40JZIacVQQzDOAoOUk8McUMx2czi6Nxq/GNTFxZltHoAioEeFSH3JmC1xiG1JmGGDdQ1jRp1exoeMijHBIm8qUlX6++igqthZgc0CUg4QfyFoTKQMMqhMvBuUCjXPZHb6zQ11+pPhTs80YUWX3lsGfkCfC3DoCc1QTUhgGPH/KTREetGllS5i+7eV9ucJFNvYbMMFR4Mk0H+PnMx87P2ebSxkDiupAWyB0qb6CUorCIore/G/QooUEEJom4Xsb2je2wGF1bKtd/AkoBVQL1OKhEAUEg9+TgTSvDCCkOvxvITRippmF80YD1KoGIBteO/y+Yos2NaBApA67jwtMkmEEGM1eEVaZgyMxVejtH/2lK0X+Z47k1lFF1wuahoDIDDBdeZwriqI7G4xxhX3D3smmLvmwX7JNqs9bcaWWkrSZEx+tSuKeUL446QeliLaR9ri5egZpYAjWxBGpj5aisnwQR1oCwfmREfGFRtFGZUVckK+STkhGvFQxwhAcvb0IQCs90YE2JwK4JwOhK5grdoyh0j0EaT6e6KCHf0Q6QNfW+4FhgwLry/YCYQ/rH4a+LI5rhaJHCKItXI5CoxJ62jdPOT1SVzzv7nNdufva3yJZKoIfIiQuBqOaDrqoYJQKMAuohP1xwgaRno85D7vESij6DWLLny3w7tvDOb9of9OV85Jc7Uz2n7Rhs//lH7qUggOwRhCwCQY6UK0GJZ7UNZb6szdxJE4HPdmsUuZ48kqkiPiQ2MiUTRlhCLKKBpGyXD/NNkjERGhIh5B1BCFSJ4cFEBWJK5Y4D8y68wDiBYUq8AhXxBII1tWAeh5cvwVsqakqvvPaA2Tf4ufuvvqbwbm8X1EPbGoZp4oqrrkLTqtV4ZnsXVkwJYlgFCAccStFqu3isdxSTPB/PZ00nkUjYc05ZAvmdtskCFhJysCci6+AfpQgjMPYPI9TuAYdIi1GKwdSYflMw9JevfOv6e/xLzmuEJAHCg5vPI7N3N/pHB2BQAbe/Bze/9ezGgaH0RplP6GGiKApswTErGMYfmlrh033Tq3754GYEQz5zYACubaE0OIjw3FngnoBeXw/KCA5+4857/ecs/qYei0JYNqDIEJYFtWkKhu9/CMqeLnRVCWyKHzLCmYSpkgaRGkVlmmPq3N7lnXXl079KL3hyS3H/P9vFUmkGL19crcc3u8IDh0CaCtyWDKHBk2FTgFAGoqko7t3XeuKlF78WvHDJ5PT760FVBWptLbSKckjh0GF/+8Uffzd9z5p3LtK5vA70SA+AylgMLy1bhirCYJoGQrPn/CFy2aVXcMMEi8WgRCIQjg1raAjGwEHIiQQkzy2N/urR86RQaK3U2gJrexvkyY2Innoyxp94Gnb7ARiU47oTNORVBsYFLMHhqDrmDeek48LDr6+oj5yzM+1HwMOQP8+/olr8zxwCDgH8HvD5tA+fnn8mEuEYDKMEc2wcpL0Tcm3VQ+Gv3XyrWlEON1eAnUrB7O6BsC0QJsM3ZyboyCjWPPiLKzJnn/PC0W2IFFRULCmvQm0gjKxjg2gq0us/eDB65pnLWFWFZB7oQi6TBmQFvpoqqPV18HI5cN3nU8sqfuns2n26qG/IWNt3QgpFANuZ0L8AHErg0ImtNEIINMKg2XZwU6P/8ZW06Zxw0svrLn+wUtJ/ZYrSEMBhUMBveXggcSJOGRuFUDVYfhU2d1AoFqAUC9ewmdNuze7YBUIBfcoU+Osmwd/YMEGQtgVBGfIrVq6cdcopLw5WlIN4R2SL1OAPYEmwDJlt+2Ac0sfCtteP/fZ3vym74/YvyhXlCMyaDqIoSK/fACuVhqRriCQqED3vvNk52/0l7xu4lkiSAGMT4SQAD0CZS3FOiuEXlSXEOYMQgJDptyzOrryNHHAvps76u93673aoARgUsGWCz2Q0XNttoDmoYFwGQlyAgEAUipCT48dXXnrJz/0XXgAuM1jJJDLbdqDQtgtSPAo1HodeX4fChrW2NTL206o7viqqcvlj95ZuapmKmlAAkqzAd/jcpY7izl3/FFi3frHvrDObzQNdKA0MgioyquYtgJPL4bUNb+DF3p0wh/o/87Ut4+m6sooHANF19M1dwXGZHcPrjOAALcDnCBBFPS5kioM3ltimJnuUPjUmsDyUx+uOhvNJNc6Xy8BLO2EQccjuFRCeB7VnoHnjzk2/+zrtKZvTtgqXnXA25l54CfRlS2FnssgfOIDch+0o7PkQheWv3i+pynsjjz4G8e8209ijJ5wMTZYASiAxCnZoEEXJF7ds2ydkeaneMkWLNjQiEE/gvRefw9efvhv39m7AzkwfYnoIX777kYWhxqal1sGDL+hTmvKlrTvgpdNwiEBVWTXOLmjYplhwI2EEiFKbFfzSWXJffG51U9dvF33rha26jBvfH8AZchymX4c9MgKloQFuKgUpGkF+5duR0Omnvln1pS9M/+P6N/BU3wd4oWMTetb9Ba0sgPJpM+CrrYF/ciPyz/9xudlx4HbOqGO0t8Ps64PZ23t4sC81T8XHnqilFDyf78Luvat7xkfrn9j+7uS7f/8w7juwGnua/PB0BTOHgOdvvhcNp54CWhaPlLZsnc1i0du8fOEkp7e/jdtOxq1MoNzwsHjrIOprm7E3Im85mMxUnK0nF2WLheZfk0R3U6hy1+wNe6FGoxCaBm9wCFIkUi1VVvxUb276JzeVWhr93PULIi0t9Jya6di48X20VzFsscfx5zUr0bt+Lfo3b0bq9y8+6d+47YsIBYpE10EkCUSWjxnsltbpn3gCAK4DPW8MvJMfff6u4u4ZB3RjGvH5wB0Xje05/P7GH2PmRZ8CABR37kbg5BMnB087pTp01hnzpJqqS+z2DsKZPMDNUo53dGNabTPmcc01RwqvXmUMxcd1d9avYlW1D4QWvBB8drktKuKAqlQRSq+PXnLRExW33nSOPmtmtTptaqPV10+1+joEa2pwul6DVS/9GeM6QzGiYUPpIN7JDjzSUJBuXJC3bdevg6jqEU1+dEj/Z4CFYcBRZEy95x5+/szT3lq7f9up44pVF90xiqeX3o5TbvwCAMDq7oE7moR/4XEgANxUGhAiEr32qiV6/aQvSZHIIhEMzHfDgeapWTtx8qhdGc4NnhEiIaPk1CSPZ+GmiB64wHfqSV8Ln33WPfGbvrCMxaJRPp4CjUUhx6KwO7vAPQ9yLIZYazMWFBS8/s5byDaF0ELDT0hp95YFUojPHS/A9akgigxwPgH6qEH2XnL5JwLm6TQAAv2px1FyPNz34APVa3eufOFHzWecfPXjj0/ISdNE+s23ET5jEaRQCNx1kHn6ORQ2b4U+Yxq0ebOhTm0F8TjgOuCOAzgeFzIVjDDKOIQjEapUVIATAuO9NTC7e+Blc1CmTIY2Yxr0lmbwkoH0qrcROfssML8PALD81tvx8y2rnp05Y8517aLAL6YJnD+YgRUNAj7fx66wpOYKn3yk0uUTDfQvfw2FC1y+t/vgHc1L3m6+5oqTzQOd0KZMRv799fC1tkAKhSZWu7MbuVXvwH/WYsjVVciufBvK2o2gqgKnWILvtJNBHJeScBhuKAhnz14iVVciv2IloKqwenshMlnoc2ZBCofgGRNbstSnwz97FvJr1yJy3rmwenpw3oUX4ZSW6aPZgMapAFwI2IQctpI/9hQPH8/8Z2eDIQDwP78ORghmE4CGIwOYMhlWbx+468LuOAAWi0J4Hghj0FqaUfXw/ZBUFW6hCKd/EF5yHPKs6SCZDIyt2wAQuF09oMEAfMcvgN22C9y24OVy8M+djfzq9yF0HerMGZDLE4ctWZ7LwenqQWnPXrjjKShNDeDdvcNVxx3/H8rPJ0Jqn3vS33a2lHOwWdNuDp179p3ewGBt6LyzQfx+8LFx+E87+egNfEAIeIUCeL4AuTwBe3gExU2b4Q4Nw3fi8eCEQtI0WJ1d0BfMBdV1KLEY3FQaueWvIXLVp0G1Cc+tsGEjqN8PoipIPfM85Pr6g7n31j2ghIMPVN5xm+DF4l81f3Zr5aS/4diwAFEkOBLbXNq19wk6Pp7QZs+aT2QJdmeXMHfuLqrTpymU0on8IQRMVQFJQubt90ApReisxdCmtQJCwD9jGpxMBtq0VrgDg7AODkNOlEEKBaG0NINqKgghyCx/LecMj0pSIkGdgUGIXP4FY++HS8yS+Z6iawictBDCtv8LAB+qzyIaAdV0m3d2rZMrK09WZk6vT7/8p3eLW3dcRorFelZe1iKFQod3hoobNiHz9DNQ4zFwQlHctBnCceAMj8Dt7QMYg5vNobR8BSghUJungMoyvFQKww88/G5p1+7LvcGDtb6Fx081NmzeIlHpBhEOJq2hUUwAPuGvDun/LcA8HARhDCybNeyDI6/kdu9tdwV/Smuo24Wx9EtDv3lqnygUW6RYJMEiEWIPDgGmCTa1BV4uB/j1CcsqHAJ3PbiFIuRYBETVoM+fA1Es8eQLL+8vvLvmrtKu3XdIkciIOzC4LdvVtdPZu/e7eqJixIWANZz8mwH/fe88EAKhyCl7ZOxJFgtNGHmKbDvZ7HNjv3nq5dy27Wermn6m77h509TjFywgrhsQkqTAsiS1vg6uacItFl0W8NvO2GhRCLFt9Je/3u+0d65OZ9Nvlp92akmKRSdKpGV1WGPJDt3n/7/1zsORc+NElg570ABAZBmUUotEo68V3v3LSm8sGZdnzaiksXAsvXb9FJ9AeaZQVKiu2SVKxuKLF3UKwxjPv79h2OnqGleqa1ym6yCMHrGLCQGVpAkx8ffM9/+/mfb/+PVvAwAcwKhy6W46IwAAAABJRU5ErkJggg=="></img>
										<img alt="Logo" src="data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAGxpJREFUeNrsm3eUnFd99z/33qdM29m+K2l31SzJVrFsy7bckMG2XMAFh2AwIby0AAkQSN4QIO9LIEAwhJgQ4teY0HFMDDbuNhgXjIVckWRblq2yKrvapl1tmZ2d9rT7yx+j6gYvKefkxHfOPWfOM888c7+/+v397h0lIvxPGpr/YeNVwK8CfhXwq4D/Ww0lP/3eb3Gby8S269C6lyTMortc1O4iTt4QN2ts0uia9ROL9KnmPF1LZtuVutksXX176hpt5dpbniXtjh56VDVCX7kWfdYJUAvq1xwHhgY64r5tJ1XfIa6k47XJ3mifX6sEydbiumRlfnt65XtKwVPPEz32GLqnAToF6TVYHaP7JqESgla/EYnz7xJXDFjONPPiD6vV+YvF6lSC8nQO4rHNfzJzWcOjtvvctxBZOLAWISHnzMGvKMCtXwwMtqNRlVfPfJ6W1Kkq66CzmlinquqUFk/tnb5T4vBbRPHP/r0a/t0Ai4CQYp77UTXX/6RKqSY7o5ACSMmi2xVSicWZOfGrqXknD4kER9iUg1S3EYTbQB34eaWRpDKaPMe1yphrcSQrZRCRtG5Q6HmNvxeq+89PUpUbSfyPYqWK+q8CnAjEaoE+K/MVdWL69+Rpi5RBtYLOgJQUuOC0Zifiu346Mr39FpR3xOoScHo0pk1BcqQQFU7K3UODXxLXydKqkBKoDMgEJMUg55zkvk+36JOk4rxDxqo7EOx/HmAFRIJq4AP6WOcq1em2JHssKEG5CpUcuMeAziqqP9r3Ibut+ogcvH4QVyI4OQ9tXCR5Aa2Nw3WSlD+sX9twczKtIQUYBUZAK5JR0POcU7RlQ1JSX0Lz9yiC/z/ArvdbaNWFEIdZ3nud41PXiBU3+HmCnqswHQqkLjrlK1QmIdxQ/Idoc/kWOxLAEY8XIKPAZBQWhY2hJs7RUt1f+0ngNH+x6Uz7VyankJpAFlSokLIQPWZxVuqs89rU5+U45Yd3JZ/DEv3WgGVg12922ZSDyoefYWnmU5IokhHBWa5RdV9GqkBNSESgoP45n/vzj+l3eiJhcki7WimMo9hdCPnsr2cItz/FXy7YwjI1RlFSxEdkyH3b2z6d69rn2k79MRWC0oJKqbolZRV2RFCAns2nzHHuYtkh75XIlo9ynZcz1PCU1CsgBWWF6mXpd9grZ1+f9NUDlunRJIMW3ayQqO6XJi9hvLl2lQ1zn2td8TlRpgGUPYgWEH45GHDJ7UOUx0tQCWiaBVd4W/iw/yjzzRSRGGpWM9R2PMsmetH58KPh25uuYsbL4AkqDfEOwVmkiPcIOq/QcxTyyNTH9Wb5+3jSRX4Ds3CSVS0vjzcSrGaFXJz/ipQVUqtHYKpSBxsLSmtU2hI/Xr5K+uPPqqU5sCHYGmDBM9RqCZff2s/PB2O0UWBijK5RlEa+VVvDD8KTeJ/3a/4o9SSL9CTDjuBsLeOR/5qcMj+K5+79B8H4UgHdUbco06NJBixoMOc2fdkpxTuigneH/AYtO5WTsy+rXZ03WXWsuhZftSc7EpxFGluEZFxwFmskUCgl2J/Fn5Ch8Ms0mKODnGf4/qZRvvTrCbZPRuBlUPqAz6PqAlE1Qjyura7lxmAZbx/8Hn98kYPrBWivh5Ss/npx43MVc3rjdeLplO5QxH0CRYt7rCIeEIg18WnmKwzGT6iq7Hsl/uhUn5p46U9CIfv7re/Rze7ZUa/Fma+xk4ASTJci2SvYqq2FO/Z/ODWc+Q6uPgorvuEHT0/wx/ftRWmHrOtSVi8nfYtRFSZjn5/p1bQ0nc9lpV5WtQWgQwjt92uPBkN6rnuDk3c6tA9iFPEQmGZFMmBRbeoY55zaF+z24AMYFb8slza8xMsaTIM7T83RfxUPKnRGQUrVzdko7ISgGmRSb2n4SNQXf+cgYToE1zf8aMskf/nIKGnX4Du/LWVPcInY5y3kyrEG7q1ZTNrD9RRxkfuTZyp/iFQHREAp0E11DkRaYYdBLU6/Rzf652rXRfsvM53mFC+cJuvjH595r8o6s6VkIQ0yKdgJi8wIJDIdP8mVTGS/pfyjtZZ3Y3707Dh/9cg+0o7C/A7lSYqImufwpgef4aafbyKIDKm0gkr5fmdgzdlKGgZtxWInBCzoHOCBlBVqifNxqhGELz11XC1z1KyUsabmm5XZy22hHhllWg5RFN0CybB8PO5tuv+AMx4ajU7A9/qX8snHZkgZMC9rwlJPaUeHDJDD9xutqVrLW6+5lzd/fZxCTTCu4GS7+9imPqSzkqiMAg12XFAe2JJAo14tNW+VHclhR7Mvmo6Z578oWGHU2eRYyKSAqyAW0CAFsEHwI6mob9bNWB8ouaBJ1/jB9GquKp1D2oQYJUf7dP2NC6wCdUzsOPMJ7QKMtHu+K5qo3005e/NdC8ejON4lIk8cKE/45Y6AT/3rPq57vcP0zg1E5dE7TVT5hL+g9Wosh9KjLQi06QY6/YtVymzCvEQenvz5/KPRWgfT6n9fGuSddlwgApVR2CkgSMajrZNnSE12EjkoNJYKaSXcXDudq0qX4usQQ50YVBLNTKlEJGZ+kJh31SL1JhRLmrNOZVFHo8lJ7cFFDUwaSLq7506lknLY2NKRliQc271tyw2TE2Mjxhhqyufk8gYum7mHqnVAFCbl5Jsun/OQbvVXoQXlHpC/AgMb44ejs4HKC4sMJ5v+0yPguyS1wZ4guel8GU2jsgoBlA/KCCh9t9INO1UW0IIAWeUxkaT5x6kL8HSMOcLMtYLQmveVIv1la1XT4lnZ/jXd2Z/MzXDnzv7BwnGrz3N/dM8DhS3bBse+8tm1ozMDz9bc8ZGwtamZ7jldNDU3opUmMGl6xvbRGuQITOZASSlF+sIb6PRW6XZI+qROcy2Iw0pdjU4llIdfBDgZrhyhYAfrBd1qtpljxwR8hWoAiUC321q0MfmWyriH+iSOshStz6cH3kAkBk8lR4GthfbjxdD5u5SngytWtN4wz0tuWr97bPL6Ma6II7V2brBpfGHXHHPlskUytHNzLZ/2h2rV8mcG+4f2Wi0IQoIldCPMhCazPUvKTR/MZMgwPyifYf8iGdNdurGuHIJ6hcBy/1RiXgx46uFPH8WsdFf6zNyqhUhzgnJBQrC1ENfvfrJl6cceFRUeDiyuonco4IaHB2n0wqNCmFLMq4R8DOCdJ7TeMPrEui99effsVeHC5ruX9OSnL+iIbk4nuY3P9m7c8+jg/IbIOENXrGrdv6oxVxksjWNEow6s1ohDLS9UVoxT04eJkgR2Um3P3qDmt35ClCBlqRcwAsxPztJT4dUv7II45m3HHOXCmbR+owwkUAXxgQgcR5PZUfqmDn8O+ogi1li8qTTamU+iji5NlaItSqL2E+e19nkDT19/+/P7XVae+MM1c826s1vif/z5lpFky9DEJctPOv7CNc1+3vW8SsZW/2b/1My4k9IH4zYC5HXIca07iBpbD3dJqFN1XdC3Op79cNKis8SgDJgy2EBfUppRy2Ktnj8SsvPOmw6TkiAUvnypn/TPeCz3Y7rbBNcXpqYMNz09XlDhTegjJGYkpOAs4A+Wrcbooys03+j+e3dO7kwpWbRlcGoHx6750/ntqenTvZHrvvDdJ2vdF1z5tQ+eVP1pT1o/sHnjukdXHL8qbuvsKg2NDmOOqBg95bGn9Dz/NLMdrTMvSm/hTEq/c8pTZ6RDqijifuHBYZeOudq+/SHx9yYB3hHp0dkQZQ9pN3D0mrvK7pnr92f4/IpplgY1elMuG3cqvpia9WcT6fgezz0c68WAO6PI/evnsOVavVg/WMsH1fGV577pK7251dc1zT32itMSPdDcknYfWvfYAyw78/NXLJQHhp98+G8rs7oLtRiK1ZAmkboZy8EsphCruH9sHdsKk6BfUNnZBJpnrd2+tSnzDTPBijhif9bwrUIDq9pDb6Cl80PHtrT90ZH7Z47eNV5/eJwg3a0r7tvf7A1ah89sbKSrOceW2KUybonyzupiGJ+oevufJjnclMNVjOTGYWvtRTmv6an131j92pwpdh7/p3PD4r0TiXWbehad+tb5HcPPPb1h3n0/ubFw1ppz6eiYhWPMUTxGgBQemzoeI+5MWM6qF7oMSSxqV78645GKz7cnc1SLiqGaYaP1GBywdHfMOenqs/6QIDlsfSr/f04+QB6E0Etd7XrpvxAFM9qpJ7UkQRlwrCIZ3/Y+u37vt7FHsAkBMsAgsPtowAsWLGb58uW88fffvnR/NXlvYrxVpSBK+4X+z5Xc1tcvmtV8vavsBs/z8dNpZopFisUCWmuMaPYFk9zRfjeSitAvKHQPAD5hZx/3hNrvOj5dY1fNp5IoDAlKNFbKvU7/M6tQunRIw5mGWaDq61fKetMS1et2bVECyq0HB1GCap77GX1O68MW1YuAchXa04d6+vbrW5DR8uHUpBXKcRBhq1stfKy9qTl93y/uWfqaC97YNd+Rv7nh+m+6vb3bcBwXESGMQmySoLQm5fhMnaIpLDMQvQRFFQvppg+6fkOXcoRtMw6xK+AINjnQCrOKXCqFKI0oUErhjO14HLGCBBXo7J7SPYuxcXRYe4pDvFc5me6mVmdFnqgXV1PaXWZyw360qxENnblmfDfP8Og4URQBQhSFgCIRiCzV0X0jm5pT/iZR0LdnNwN7++oB0BgacjmMMRgNEzMFXHcOc/wmRB+dAbQoQpJZE55/vhJBWUWiFaIVKraIUsQ2pjXVXFrbfXFJtEYnFmKLY0sFUB6Lm+cz6Hpbq0kMB2+QulTUAfAJQhKE75Fa4TZ8DTsnSB4bOdRtbVqymMY5GXrmzqOzbRYDQ/3s3LGZbVs2cNzylWTTPs2NDdx//12sOulksrkMRhuWL1sMWqiULemUj1Ixw/1j5Ec1dpkDsT3CtwXrKJSXebfSZoFYe5iwywFPUyDGMFErjD/pwVQyg7tpL7V1+9CkU9A5i4tedyWNXYsasBZEUOrAF9Xh5rtG7LTxLxzU2Y+MWpdqfDQ7T5KEJLE4joPrubiuQ2MuS9/ubZx16krmzm6iZ3YH//L9f2Z47w7+4eq/Z+H8BbiOi9Yaay2JtVhrUVpRlZjxUpGJ8sxRczAuXjFmoz9HbLW+UKlr18phixQgjszxmTlePkiIopAwiNAKhUbn7x/Y+MmCrXwBR8fGUn/AoU0G0AKxsnTZ7JdaGtsuVo77EbDqpTcmpO4mAlZAa0M6lSKdymBFoZVGUCxZvBjHcbBieamjF9qCMhqt1KEZS3Lqye3Lls7PNr7dOuyWA9YnSqFUPZWhNSixpNOnbioP3VtSyTJHabQGPYtZbWnTdMG2ZKKnY3RqTdNM5Z+SlMY6msTTdUGlHQIRKAW6a6xWaHxq7EIdl54UZNbBxWXSGVK+jxWLUgptzIHYYpXRpjkMQx5e/4u5lWrhk/Pnz/pqa2v+LYm1RFHsiEj6oIAO4hZX4feHZLbViB1LJDERMbVK5bljCk2fWzWd3SMzlZ7Y1k0cV2G1IvE01tVgw0e6to8dMzqz/6G4pP4s0GZVgnbN6hPXVCcaZKutVO/5wx/3d7x2IPjgdKPfk6nENJUTlgxWePuj+/nMhmk++GyFM7dOLnj8170by/vl8ciqUjJYIZPNsPqkk0FDYiMc46K1NrWg8v6GvPeDzlnt53W0df3wa9d89bxUVr65cGH36el0prZlc+9tnZ3N7ylXJ2+2VhYniTzluqYk1lKuVPE8Fz0YcO7is3j9MWdzSsNSlqieaGr9rsyHnpv+2/c9ue+Md20rs3xnkWqSYBJIBwntMzGXPjUZnXL30Ob9rv8dE6t7J4hLdnc5VmvefTHPdpbpuL+Xto2j3hc7Zq9fo/1T+0mwCmaLrptWoLA5oXZhjSdMpfzpf9WX7EmbXyIJC+cvoKO9g+HRYVCxrtVCW62EzqLF3ffN6Wo9Z2KyUEm5uTutxHMdlzNd12W6MDOpcB/o7plz/NT0/qWVclDYsmXXa/MN+c3VatUbG58KG/NZkijhoosuYemK5cRxTK0U+OlffuWOS7VzYabg0zxgcI1CEPZpYdpAV6RwRPgTNzlnMOKXg2e30G8qyP1DmDmnLcUWplj66D7clJfsUzJ5Rir1lpzRZMUQxoqwCYrHhexdU6V6WpVcVXtn9OZfEyfR7UGpWGxsboFcAzMzM+lMzrsuCpN8tRpMdnS2vMP3nTkZz7hGBStSnuoxCNiEbMpJpzyWl8vFdsf1iKOkXJwur49jobk5e0OpXN1ilN7np3w2PPM0t996C0/8ah1v3rLjaxe8JnlbYWlMbWnM9D6NnnJQRpNB06IUGaO5Xcc3PWnjqxuBsflZCiqC3TOo837/Qqb7B3AGp8AYAgUfEX3b26r+5WOLYipzQuJFIWPpkMl+Rce2DN3DKWZjuDEq3/j/KsU/SHX30DBnLmEy/ePZXS1vKc1UJ61NkpbGdHtcCyl6rYwlWWrpNhIng9IGVSuQre2n06mQCycxqTSV0IbVSm2ipSU/e2pqZvy5LXsuzOQaNsWj+ynnssybmLr0tmzznTM+jHcGqDMivHZF2K/J9hkad/o0lAwjOiyuHR86fVJkq5NYwrNnEc9txN47gOPjoBOPmcYGtNaEwI+j5EtnnVm9eKqn4gZNMWbEIfVwlmWDHm2JS9ZASiyvd/wr/6W149pN4xOPHJvJYFz6bGJpbMy26LjGUNLErtmnUe1cTqq5k86MoVCJ8TREApHvsHGgD298B4smfk2n2u9lmhtmC4pqJegNa/GO7mNmc6FyOfmarzL705//WNuWXhrFp21vlomRmOH5NeIVMdWTY+ScGrLT4ZuPtlw72Xrc1pRYSISUyVCs1AgMOH1DoxSrFaIwQh0oox6L5IlvheVb3lw2V7I+y6yRFPnE4BlFWizPZBS/6k7zhr6q8qYLXdnWhktrQW1bV3vrOY7R2EqZZxpOpv+Yi1k1fzbLGw0ntPo0pl2GiyFKhJ5GHxFhz+JGHhhcwrbBkxnZfi9LC4+TzmYxjmlyHN1WDspv0J6Zvf/qf0zcrduPvWN+Hhtb3jAa0Z445Lfl2L8jZvq4Gs7FARvjcP9N1fj7zW1ZjK0XOaoAlckSYZCgaGyq92Ne0FI9AeeEnzTkHm4LTGN9o17IRZZb2wwfubiTyZzHdTcPx80T5YE7TlrYM+CZqfZcup0g5tmmM5g69mI+sLKN5a0pFrRm2BcqUhmPqFYhsZbGXI5iOWKWL1QqAQ8PlLizr8z4pp9yyr6f4WUzDBcr+xqszX/8uZHMacUqQymfK9e28dhxWT7x03383+1VtKNJlCIJIEwlfCEq/823g+izmSNKL6UUSZRQrlao12RR9KJZioLRkWo49Npc5tJ8IrqaWK5ZnObDl82hlk9x/a0jvHk01Cu000wQ6a5Esm6pzEMdZ9B/0h/wriWNnNedwc9n2LBjL7133ErpRzeTvfsukvvuY/eD63hmZy97nRzZ9g4W5RxO7UyzTi1goFiha3wrZxaj3OqJirtyuoZ2HPJWWDYecfPSPA+d2MiOJOLMkYCGGCSluKlcuuPrhcKfleMoiaKI8MgZR8RJ8sonJZa4Ln/R0rF6U3fqa4+t7jh984IcpDQf/dkYV28sUvYVAxoyCXTEllBb1rf3sPWCy/EvuoieBfN5/Lvf5bI7b2DlvmHcWNBSZ3AxQuJpnm9p4/bz30zPu99LZy7DPfc9gr37dv5y2wPMDeo8ud815KzQAmRDy1XL8nzmkk5QwqzhGv/roeEnThsOfnh3eeab3ysWgt90kOFlx1LX4/3NrXzq7KZ8bUX7DUk+fenK3jL33L2PFgsTGiZRLFBSLyAE0nHCdFgjOG4J9zd2sPL5Z+ipBky1ZHAXpql0ZNAipMfKOIMhZqxM5DncePp5nFgrs3z7s3ROTRB56XoXPgFJYCCjmJMIKQsFhEsu72TzwiwEM/+y5OnCB77wcKV6b7XId4qFV9zGMa/0YacxnJXO8Ku5fqCeGf1JriuX+99bJ884dSQhVDAsinmuoC7XSAgyBpGrcV0Pf2SClX0DZI1iv+dSbPZo7QgxYRlVK2MyQiXwqc4kaCWct2s7c0f2kEnFhJFPooEqqG5wz1HIDhi1iqyj0OcmtDTH1Tut/gSjuz7RMZOJzxmI2BkFPBXUfvdDLZPAQ+JQGrJkJ1S0dt3U9y49o/L+yZTJxo8LtRVCVHRxGxQyW5CtdY1YAXWST7xE4f3C0jMhmP4y0S6Fj0IUaELaTACOQmJFFPvoKzQqB3KXhSlQC8B80MAmgbwQL47Z3Kej2JMnNo/wRRYkP0XrIzanfvN4RQ2XtMNzqRbi0Zhq2dA/FozNazUPRhpv1gArOT5RgYLcJoU+X0OLQvZIvRkeg1qiUMsVzFMkrgIHdAOYnEK3KiQD0qZQl2j0mQoGQPYIul2jOsG836AeFEq3WZI3JXQ2Wb69oXjNrQXnrRNuqre/x0eqU7TPpDlnb8TO+N+p4UOic+qdRJNSWJwnbnt+5vmW2F2xcL276guzgx+sHnDmveWH7utql2jM2zTyjGD7hOS7FtUIeq1CHatQazXsAxkSVAaYrZD9ddXYBwS7o35dv1GhX2OQ+yzOg5a/y8vV3f21487a5V+SEf+OlqqmbITf5Sy/eeWjpw6k8kdtn8wxNTb3V8KiJEm3kzpxwwlr3rVuYOeDpxXNZe3bVD72FObdBtUIlAXGQLaD7BTkKYEJQcYF2SbIc4JsB/uEQAzmNIW+SINWyE2W7AbFXdTu/qLH+wZHauuGitze5fsPTwWK8hyXXbN9pFr4j9bw4Xhei+C+Z4r4jvAA8febnYZbyKQLWoR73PDNZwfODSvX60XRoEZdoDFXGrBgH7TIsMAcjdICewRxQeUUajGoOQrZLahTFXKr4ExawgBu1tU7bktq73JUQ+xoNVCFATnY3fivOGupFKTc+o+lhMQXCojFAElin/hGk/e61/j5vz5/uPiOhm/EmUybJurRSDNwskafplCtGjsA8kSCfp1BtljUXnD6QJ4MqGrDHt+MPOVGf3d9qXRtt+vG+oDA9e+K8j/sNO2RB4zFEnn+0NPzu//YrOn5ktxy23s7ksbXzH2mckpnSufyiSJ6qN7aNRFYBL0pgamYKVEMazuSHLvgkUcG+h402BtFq+ms+t01+Z8OWFAoEVIC2nH6tqdTf31za9Z/z8zUvJmGtqYvV8uzL4qiWUGoGmtiaXGdZGBsejK99sLB39u+feKXQ4OjwSknDKuJ/dJQmkEZ8590QPzV/y29CvhVwP+dx78NABZ9P15+TTUXAAAAAElFTkSuQmCC"></img>
									</td>
									<td style="width: 640px">
										<div style="text-align: center;">
											<p class="toUpper brandTop" style="font-weight: bold;" >ບັດຢ ັ້ງຢືນການຮັບວັກຊີນກັນພະຍາດໂຄວີດ-19</p>
											<p style="font-size: 16px;font-weight: bold;">COVID-19 Vaccination Card for Lao People’s Democration Republic</p>
											<div class="clLine">
												<span></span>
											</div>
										</div>
									</td>
									<td style="width: 110px; text-align:right;padding-right:10px;">
										<img alt="Logo" src="data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAEg5JREFUeNrUmnmQXVWdxz+/c+99S/frfU1I0t1JSAiEQFgVBQLKgAIyyog1gkU5YlmDxBlwmbF0pmocdWqKUiydqRkRpmQIiKOCGyhGREBAIAElYcm+died7nT36+633XvP+c0f76WX0J1uYsJkTtWpen37nnPP75zf8v19f0c0vwEAjEc00k9x8BXUlPC0hmRNG1F+D8aGOIkhqMFPn0p84Inz/bqaJc5yis0f6FQbJcEpSHkulPJvrfwthz07/L2JbYqxzlEYLYnvVZXSzfN3ekndGI4O7Ui1XfJ8Kb/dURrAqIcGAUH1UsJ8D3E8jPGTpBtXYFKNoBYAn1k1DyXqMtHgjVr4xXt9HTxNBnJVnjh8BBlbs3BcmijGFtHYIdm9EPgkTFXB9v3iFQmaf+aQ7yLe7tlMdWSBxQOkTWz2c77dd4OxI62CLT83MjZceSuaKe+n8RDjgebTUho5x5R6zrFe5hMatN+H33I74vcdncBi0NLA+/38618zbqhLUFRM+cMnRDMgBlA8m52LHfmsjfquUb/pU4j36BFGTanCYuyBL8jQb39g3GAXQkXYE6C9QZ2krHEiePbgEg7+4idS6vkcEsjsBBYPL85+JQg3f1kIPU4UQWdl6z5ClPTzG//VlPq+VDbJw1X6kEBiQAK8aPDzgd32eUEZF1b+HwltECzJcMsXoyA1jAluP6T6AEbjPBrn0aiARPveG0RbvuSpPaGEVNU36RoNRmL8/Cv/7HJ7L9WogEY5NMrhR9kdgKDqGiT77Dc9U/J1ttHqBG6Kh9FS0vY+9m+2VDgfMSOgGBeVcHEEhb2f9tzgIk5AYeWox3l47uAySj2rjfExAsZPN2OC5Fwpbf+ovEUR9Zg459mMkTJik8LWTxoJ2oNEIyYu9uMKPdcZzc3VE9Uj65/mxIzLzrX5ne8LC/2YINXgeVH3NcKJ5aiOpUEIDsKea6WmHT8c2brcd7lzZhdvBdSBhqCKIKgXjIdzdaiGlENagrE46GJQO54OCIj46BRx8rihMjdydjj46iLfT6QWSH44U8bGM2F4h5Mk1J2Pn2rAhcO47HqMHQEUZ9KYhkswXoo4+xIm6kMVNN1JULsCUi1gi7jcduzIRkw8AiZ4Cw5ZMPFQU+CZpb4J6haiblaJk3MhNF1AOOfjbBssMKclQXWyDYaeQwGTOZ1c07XkrdCWOYV4+7/jtb0X034Vu3OGvlJIYISFc95NpmUn8d41SH4rmMRxNOJDU1j8dPM8Px7e1uEbmbU9+MlGto84bvvpRv7+ki4u7LoKbXpXxT+kufelnTy7P8vdly0j6FxNnFnB/2zbw2M9uwiMIbKO5lQVNyw5jTO6VhNvvR2JuoHgyOIKqB69HSMGze1cbEr5QlU8WiIeDWfoJaLRAi6MUXVkw5jIOsAgphox1QyGMd/b0sNPt+/nD/0Rpu5sfrRrF4/s3c4V8+bzd6efya2nraAq8PjWhnXsCGvxmt+DCytiyTH3V+XdUkVUifLDSd/FVmwYozJzbFAbo9ZhRPDNxMS/3H6yZT/bh3K0VAXcvWEXS5uq+HX3Hq6ev4APdS0ee29BTYZ/fGkdv9y9jb8+eSWlTUn80gheKoUJvMmExyyAiE71T6vYUkwcxmXVUEeYzKuPEaPyxsUf6Us6xafyseWeV/dwZms9Z7bW8sBr3Tyyez++CGc3tU2apj6R5OTqGnZls6h0YbwMrjiIi0uIL/jJAAnGPbhTnaAAimq5y6EFTVi7xootRdjIgnWMCVaG4/on4EityFvmoH65Yx8v9w1TV5Vkx3CBocjxo037qK+D4agI1E3isEatpSrhI2g5lFXCokZKGIcYT0AE0UPJQ1kym4+wEk9iw2TCaTlryz5YZNLz2VE8M5iG8aW8COA7G3dzVlsdnz13CaA8truPe1/ZwzvSKX6+ZzddtXU0JVIAPL6vmw2DB7lp2RlQ3Afx8PjypTJ/rICr6JCMa5cdz5x03NgmhaCynHoUnNaUogqKUowcX3piB+63WxgiZsfoKA9cfTZ/tqAZgBXNNfx48z5ywx7ZTMQX1z/HwkwNo6Fl6+gwF7bN46LWFoqbHoB4FIxfBjVj6MiUT1EURcuboGYaxyYzmf1RCqyCtRENKcMNK+eQtY6hYkjkKTfWn8RFcxvHtLa9KsWX3nEyvbkSVy5u5bn+XjYPDVIVJPjEKadzQVsruufH2N4nwKRwQQMiHoqicYEgKOIlM6ikQBy4IsYWyr8l8UbHJoKLHbYQHiuBBUSJRnbR3OlYfcHCqT35BGfyoaULxp4vqq0bfy0aoLDpm9j9TwGG5NKbCNreNm6H+X5MfgfULIBkS/lhOACjm3BDz+NyW8r2Kf4kgSHGHtMTFvAkolgqcddDv2Pbrl7mt9Zxy/WXkkwmyqol8MfeIdZu6sc6ZW5NQKJK+E33AAAXzG3ixsVJ7MBGxBXw2y8hbrmUr/5+K3uGC8yp8rnt3CUcTM9hfW8/vYUDIEJrKsWKhotZ1nkx3sHHiA/8BONKTOKt9BirtKhgvAShg8ef38rTL25jxeI2Pn7dRRWBYdvAKHc8uZsdQzGNKeGLl3aQSXt8/tnN9ORjnuzJ8s55F9A5/z0Utq4hueBKfrjjIN94aTslCx87rZ0Hdu/g1z37KagSqwMFzwjVsp1Vbe18cOEVNPv1aPd3EY14M0SjOVoEk0z4VKUSpJLBWKjrGc5x+5O72DNsaaky/MO7F3L2SY0sbazjk2d0kvY8enIl7nttD17bO0h0XUcpvYi7X94KeCyuT2HTIY/s20dBlRrPsLK+jjMb66jzhByOh7p3s3ZvN17DuWiiaYKjm13zRUFmCVK1kgJOVB0FPAPZUsjXntzF5oMlmlLCbRct4Kw547H3xtPm89CWfbySDfnB67v58NJzWdzxIe5/fQ/r9o+Q9A1tTQEHbIhR4YLmej7Y0cW8qjQK7MsVuH/XTgzC1R3zsAO/QUoHwHhHLk2NM4EoiB+kEyQkNZ6bTlfbAnDgpQPCcowo75gRRiPLt9f18HJfSGt1gk++rZ23z2+cBM+aUylWr+zklsdfY0/O8f1N3Xzm3JO599VuQhVWNKZoqxOGIstZtTXctuxUkt64xS2sreHTp56OU6V6+HHi7u+VE3u8CghSxBOC6uTUuFwdNghchdmSMaQzPXDVsieckFmJQGgd3352F8/2CX5c4LrFzVzY2Tylfvz54rk8sKmHtXuzPLzzACnf8PJAjoaE4bKOel4tZUEd71/QMUlYB6jNkxh4AhP2Eg8+jSFCxZ90lALY2JbR2RTpoYoTP8yHkCvCbNgHG+KnwsqpC4Ex9OYcz3SHGBeRe2INL26r5pqV/4LnTZ7PKSQ9n9VnLuT5/X9k+0iRO17cSSFWrl3aypnNGV7YNUC1GFpTiUnKJYAYgx34FTa/C/FrceK/8RBVcaGdAEUPO2ETYw4DZrOw4kNox6DiEYtPcybBgv3PEm15nseeeJL77r//jd6x4tkunt/CNQtbiGIlEkNLyudvz1pMJhHg1BGhRE4PVUknfFnQxBw0OQ/UVdR56rx5ul7Gb0eZOPjxIF6xh1rt57MXzeMLH7ma5tZ2QPjve+5hw4YN05TphGtPbqfKN5QiyxVdLSyszzA3U03aGIooT/f1TTYnVYz4BJ23ECz9MtrwdrDhUTECRyWwqJIa3Uxq8Dnm200sqQ2Y17GIj910EyKGXC7H17/+dUZGRqYcXxUEeAIWpSGVBKArk2F5bR2qws+6e/hdb++YfZazHo/f9o7wo73DyEl/hTZejL7JkHTU2VJZaIvgUGeJrCMFXHXVVaxfv561a9fy6quvctddd3Hrrbe+kRubcDKHfieNz190LGDz6AhZ6/jm5s08f7CflU3NGOAPAwd5qq+fvLWI+Hxg3g3Exd1Q2FNOPI4r8Kgs1LlyH1NXz+Pmm29m7ty5GGN48MEHefTRR6e0NatKrMqE4SxvaODmJUuoM4aROObR/X3csWkTX9u0iUf29TLiHA1BQH0yiboSuHCcBDgeAuuhsmpFzYwxGDN5ivb2dlavXk06nSaOY+6880527tw52Y5xpIxQLZCQyXb4ztZWvrryDN7V1kpnJk21Z6gyhs7qKi5va+UrZ53NpY0Wu/s7EPaixjt+Ki1iiItDBFLibz51C8PDw1RXV5NOpye9t2rVKhobm8jnc8RxPClEKXBqUw1rLj+T2Clza5LlDGsCO9GZqeEzp57GcBiyv1BAcbRXVVMXeJBdT7T7AUypG7zq42zDJoEd2oI/upHly1cd8dUVK06fFsfUpdKcf1J6xs/VJhLUJg7jrOsWEuh7iHt+ithsmd4VHYs+x1RgdSHJ1tPZdrCZe+66By8wU37kGN/SQisgD4UwUpafuoS/fOd1RFv+AzSaNEJnCFW+MguGdiwcWhJNZ7N3u+Hu7/+KVCrNW9lEIVcoceVlWa6/8iOo34zme5AxlCjTHvOhjfPVuXj2NixgfALfpzaTJp1KldGQzHBcR/KCb2KsAr4vVKUTCB5iKlSPHKYzU45VjOfh+4l0gYLOYnXlLSoN7eLcFZfxyL3/hGfMMRV4ojbKNHviVKnOZFA7hJ8YRbyqSQSAKsT5EBe7SVy7qpKubSv46eZl20uDG5iNdxeTJOp9hmTjcpZ2Xfh/WCDPEu78IR458BOTQLcAQSZJlCuhkavsXIXIT9W/5se5gZ3Mtk4rBqNFwk13Eu1/BhKN0xBJs3Vb44vxEgFeYI6YyqgKtlDAZTeT8A5AMlkpoxy+TCHIJIlHS7jIlj/npVxu74s9fikOX/eChoNE/U0zF6gr2VJchAO/R41DXQju8GXqmAOR8QLJWKVCRPDE4NSVTTARYKqDsUrDEetb2SIQIA0NM5yNEGRSxLkScVhCg6Yer2HlK36qZek2tb0veMXhK94MJi3vuEPrz8VkTj9ElU9OnyugQwDxBM94WOsAZdvACG2ZNKFAQzpB4HkVGqa8IXbKnNbgF7JE3b8EOwhBckb/4GeS6EhMnGxYl2lftt83WGg65V4zsO8KI4rOEm2KC3GZ5UjXzXjT1HbXvfgyhUIRayM6Fi2huqaeptqA/aOWvniUzUTsHMlyQ9uiSpIPKQ+yoaUtMb22uaARt/fbeDoLR4viV6eRxuX3RfEIfqK2Axs2/9wO/uF1cf2nzK6MCM5Z8JsxRyhk19dW8cxTj+MHSdZtG6avf4hTzjsDampJpi3DFAmM8syBvQwUQvJhET8IaEsmuHpBx/SAL9UGfgooVjitI1f+rd++IWhY8SsTZDDx6AAa6zC1y7/ldLYVmkOhLz48ak1qnZ1d9PT0EEaWoVzE0oVzCZIJFjWkGS6WqPEStCSSDBRDCkBHbS2hcyyrb3gDr66TqmeucsN9psNxqHpI/co7FIZtaQDfz6TLBbL0af8V5bZ9lHD3ObOyZQkgtwWX24xXvWScXpkI43yP933gGlqbmhmMEgwMZOla1EwqkaCmupEg8DACSd+jv1ggYTxOylTRnE5OC5ecK2D7n8aLi4gXHMmng7O41KKngqbla8QIkEC0tG4s6ocHN50lg8+uNW64cVahSi3q10DQio6hOpl41wA/mawUpivJaGQr2FgmH5uRsRDjFNyUTlKJRoZxI3tI1iYxgT8NzaOIc8Re3T5puvjyoPHUDYdqYz5urKxMouWMFyPxb3b9a9cYQn9G+xAPiUcgGppWuWzu6K9nTIX0JFeaQMXptIjQSqJgmlZ93G88bwOuOE225EJM1Unfj2rPa2D4hW94lJIzxmbx4K26YKYKpqyq09ATiDqspPJa//ZP+NWdD+NKMzAeLsSrnv+fpuWyD1tT34eL4AS+dHq4yseS6aZ51Qe9mkVrKteDZqZ41JXwapc96GrPu9QlO36tKpXrgyei4Aoa4VRwQcfPbWLRJcZPPzJdkW16lOEiULdRWi+8PG646CM2Mf8FFQMalzs6ob+Fwh3qlfubTj1scuHvbc151+M3XC3ibVG1R8t4KKh1Jt2xxlrzUFxMrfKNXC929BzioQ7RYgK1oOYInuYYyTmRAhQ/tMk5O0g2vGDVfE9SXY97ibqCy/6xfB3iT6Z4NAaNcop5WOqWP6zUZOyB36w01TVNfv2yk7TUX4t1nhwnkRVVvKQTGIh2Pt3tvIYhv+Xd68UU8zr4GqJhRetmbv87AK+0TmeE53qAAAAAAElFTkSuQmCC"></img>
									</td>
								</tr>
							</table>
							<table style="width:100%; padding-top: 15px;">
								<tr>
									<td style="width: 270px; text-align: center;">
										<p><strong>ຊື່ | First name</strong></p>
									</td>
									<td style="width: 270px; text-align:center;">
										<p><strong>ນາມສະກຸນ | Last name</strong></p>
									</td>
									<td style="width: 360px; text-align:center;">
										<p><strong>ລະຫັດຜູ້ຮັບວັກຊິນ | Vaccination ID</strong></p>
									</td>
								</tr>
								<tr>
									<td style="width: 270px; text-align: center;">
										<div style="padding: 0 10px 0 10px;">
											<p><xsl:value-of select="KYC/FirstName" /></p>
										</div>
									</td>
									<td style="width: 270px;text-align: center;">
										<div style="padding: 0 10px 0 10px;">
											<p><xsl:value-of select="KYC/LastName" /></p>
										</div>
									</td>
									<td style="width: 360px; text-align: center;">
										<div style="padding: 0 10px 0 10px;">
											<p><xsl:value-of select="KYC/TanValue" /></p>
										</div>
									</td>
								</tr>
							</table>
							<table style="width:100%; padding-top: 15px;">
								<tr>
									<td style="width: 220px; text-align: center;">
										<p><strong>ວັນເດືອນປີເກີດ | DOB</strong></p>
									</td>
									<td style="width: 220px; text-align:center;">
										<p><strong>ອາຍຸ(ປີ) | Age (year)</strong></p>
									</td>
									<td style="width: 460px; text-align:center;">
										<p><strong>ທີື່ຢ ູ່ (ບູ້ານ/ເມ ອງ/ແຂວງ) | Address (Village/District/Province)</strong></p>
									</td>
								</tr>
								<tr>
									<td style="width: 220px; text-align: center;">
										<div style="padding: 0 10px 0 10px;">
											<p><xsl:value-of select="KYC/DateOfBirth" /></p>
										</div>
									</td>
									<td style="width: 220px;text-align: center;">
										<div style="padding: 0 10px 0 10px;">
											<p><xsl:value-of select="KYC/Age" /></p>
										</div>
									</td>
									<td style="width: 460px; text-align: center;">
										<div style="padding: 0 10px 0 10px;">
											<p><xsl:value-of select="KYC/District" /></p>
										</div>
									</td>
								</tr>
							</table>
							
							<table style="width:100%; margin-top: 15px;" class="table1">
								<tr>
									<td style="width: 165px; text-align: center;">
										<p>ວັກຊີນ</p>
										<p>Vaccine</p>
									</td>
									<td style="width: 220px; text-align:center;">
										<p>ຊ ື່ຜະລິດຕະພັນ | Product Name/Manufacturer</p>
										<p>ເລກກຸຸ່ມຜະລິດ | Batch No.</p>
									</td>
									<td style="width: 150px; text-align:center;">
										<p>ວັນທີຮັບວັກຊີນ</p>
										<p>Date</p>
									</td>
									<td style="width: 200px; text-align:center;">
										<p>ຈຸດ/ສະຖານທີື່ຮັບວັກຊີນ</p>
										<p>Vaccination site</p>
									</td>
									<td style="width: 180px; text-align:center;">
										<p>ຊ ື່-ລາຍເຊັນແພດ</p>
										<p>Signature of Vaccinator</p>
									</td>
								</tr>
								<tr>
									<td style="width: 150px; text-align: center;">
										<p>ວັກຊີນໂຄວິດ-19 ໂດສ໌ທີ 1</p>
										<p>1st Dose COVID-19</p>
									</td>
									<td style="width: 220px; text-align:center;">
										<p><xsl:value-of select="KYC/MedicinalProduct" /></p>
									</td>
									<td style="width: 150px; text-align:center;">
										<p><xsl:value-of select="KYC/DateOfVaccination" /></p>
									</td>
									<td style="width: 200px; text-align:center;">
										<p><xsl:value-of select="KYC/SiteOfVaccination" /></p>
									</td>
									<td style="width: 180px; text-align:center;">
										<p><xsl:value-of select="KYC/CertificateIssuer" /></p>
									</td>
								</tr>
								<tr>
									<td style="width: 150px; text-align: center;">
										<p>ວັກຊີນໂຄວິດ-19 ໂດສ໌ທີ 2</p>
										<p>2st Dose COVID-19</p>
									</td>
									<td style="width: 220px; text-align:center;">
										<p></p>
									</td>
									<td style="width: 150px; text-align:center;">
										<p>___ /___ /___</p>
									</td>
									<td style="width: 200px; text-align:center;">
										<p></p>
										<p></p>
									</td>
									<td style="width: 180px; text-align:center;">
										<p></p>
										<p></p>
									</td>
								</tr>
								<tr>
									<td style="width: 150px; text-align: center;">
										<p></p>
									</td>
									<td style="width: 220px; text-align:center;">
										<p></p>
									</td>
									<td style="width: 150px; text-align:center;">
										<p>___ /___ /___</p>
									</td>
									<td style="width: 200px; text-align:center;">
										<p></p>
									</td>
									<td style="width: 180px; text-align:center;">
										<p></p>
									</td>
								</tr>
								<tr>
									<td style="width: 150px; text-align: center;">
										<p></p>
									</td>
									<td style="width: 220px; text-align:center;">
										<p></p>
									</td>
									<td style="width: 150px; text-align:center;">
										<p>___ /___ /___</p>
									</td>
									<td style="width: 200px; text-align:center;">
										<p></p>
									</td>
									<td style="width: 180px; text-align:center;">
										<p></p>
									</td>
								</tr>
							</table>
						</div>
						<div class="wrapper-inner">
							<div  style="width:100%;padding-top: 15px;margin-bottom: 10px;text-align: center;font-size: 15px; font-weight: bold;">ທຸ່ານຄວນກັບມາຮັບວັກຊີນໂດສ໌ 2</div>
							<table style="width:100%;" class="table1">
								<tr>
									<td style="text-align:center;">
										<p>ວັກຊີນ | Vaccine</p>
									</td>
									<td style="width: 220px; text-align:center;">
										<p>ວວັນທີນັດ | Date of Next Appointment</p>
									</td>
								</tr>
								<tr>
									<td style="width: 150px; text-align: center;">
										<p>ວັກຊີນໂຄວິດ-19 | COVID-19</p>
									</td>
									<td style="width: 220px; text-align:center;">
										<p>_____ /_____ /________</p>
									</td>
								</tr>
								<tr>
									<td style="width: 150px; text-align: center;">
										<p></p>
									</td>
									<td style="width: 220px; text-align:center;">
										<p>_____ /_____ /________</p>
									</td>
								</tr>
							</table>
							<table style="width:100%; margin-top: 10px;border-collapse: collapse;">
								<tr>
									<td style="text-align:left;width: 500px; padding: 10px;">
										<p>o ໃຫູ້ທຸ່ານເກັບຮັກສາບັດນີີ້ໄວູ້ໃຫູ້ດີ ແລະ ຖ ບັດໄປພູ້ອມທຸກຄັີ້ງທີື່ໄປຮັບວັກຊີນ.</p>
										<p>o ຕິດຕາມຂ ໍ້ມ ນເພີື່ມຕ ື່ມກຸ່ຽວກັບພະຍາດໂຄວີດ-19 ແລະ ວັກຊີນກັນໂຄວີດ-19 ໄດູ້ທີື່ ເວັບໄຊ https://www.covid19.gov.la ຫ ໂທສາຍດຸ່ວນ 166 ຫ ຕິດຕ ໍ່ ພະ ແນກສາທາລະນະສຸກແຂວງ/ນະຄອນຫ ວງ.</p>
										<p>o ອາການຂູ້າງຄຽງເບົາບາງອາດເກີດໄດູ້ ເຊັັ່ນ:ເຈັບ, ໄຄຸ່, ບວມແດງບ ວິເວນສັກ.</p>
										<p>o ກະລຸນາໄປພົບແພດຢ ູ່ໂຮງໝໍໃຫູ້ບູ້ານຂອງທຸ່ານ ໃນກ ລະນີ ຖູ້າມີໄຂູ້ແກຸ່ຍາວ ຫ ອາການ ຜິດປົກກະຕິພາຍຫ ັງຮັບວັກຊີນກັນພະຍາດໂຄວີດ-19.</p>
										<p>o ເຖີື່ງແມຸ່ນວຸ່າທຸ່ານຈະໄດູ້ຮັບວັກຊີນກັນພະຍາດໂຄວິດ-19ແລູ້ວກ ໍ່ຕາມ ທຸ່ານຈ າເປ ນ ຕູ້ອງໄດູ້ປະຕິບັດຕາມການປ້ອງກັນພະຍາດໂຄວີດ-19 ຄ ເກົັ່າ ໂດຍ ໃສຸ່ຜູ້າອັດດັງ-ອັດ ປາກໃນບຸ່ອນທີື່ມີຄົນຫ າຍ, ລູ້າງມ ໃສຸ່ສະບ ດູ້ວຍນ ້າສະອາດ ແລະ ຮັກສາໄລຍະຫຸ່າງ ຈາກຜ ູ້ອ ື່ນຢູ່າງໜຸ່ອຍໜ ື່ງແມັດ.</p>
										<p>ພິມຄັີ້ງທີ 2</p>
									</td>
									<td style="text-align:center;width: 400px;">
										<div style="width: 200px;padding-left: 60px;">
											<table style="text-align:center;" class="table1">
												<tr>
													<td style="background-color: #fff; height: 6cm; border: 3px solid black">
														 <img alt="QRCode" src="data:image/jpeg;base64, {QR_CODE}" />
													</td>
												</tr>
												<tr>
													<td style="font-size:11px; padding: 5px 35px 10px 35px">
														<p>ຈ ້າກາຂອງສະຖານທີື່ບ ລິການ</p>
														<p>ເມ ື່ອໄດູ້ຮັບວັກຊີນຄົບ</p>
													</td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
							</table>
						</div>
                    </div>
                </div>
            </body>
        </html>

    </xsl:template>
</xsl:stylesheet>

