package vn.mobileid.demoqrypto.controller;

/**
 *
 * @author SonThai
 */
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
 
@Controller
public class MainController { 
    
    @RequestMapping(path = "/plugin/v1/{fileName:.+}", method = RequestMethod.GET)
    public void pluginV1DownloadVerIns(HttpServletRequest request, 
                                        HttpServletResponse response, 
                                        @PathVariable("fileName") String fileName) {
        String dataDirectory;
        Path file; 
        
//        dataDirectory = request.getServletContext().getRealPath("/home/dtis/plugin/v1/");
        dataDirectory = "/home/dtis/plugin/v1/";
        file = Paths.get(dataDirectory, fileName);
        
        if (Files.exists(file))
        {
            response.addHeader("Content-Disposition", "attachment; filename="+fileName);
            
            try
            {
                response.addHeader("Content-Length", Files.size(file) + "");
                response.addHeader("Content-Type", "application/octet-stream");
                 response.addHeader("Accept-Ranges", "bytes");
                Files.copy(file, response.getOutputStream());
                response.getOutputStream().flush();
            } 
            catch (IOException ex) {
              ex.printStackTrace();
              
            }
        }
    }
    
    @RequestMapping(path = "/plugin/v2/{fileName:.+}", method = RequestMethod.GET)
    public void pluginV2DownloadVerIns(HttpServletRequest request, 
                                        HttpServletResponse response, 
                                        @PathVariable("fileName") String fileName) {
        String dataDirectory;
        Path file; 
        
//        dataDirectory = request.getServletContext().getRealPath("/home/dtis/plugin/v1/");
        dataDirectory = "/home/dtis/plugin/v2/";
        file = Paths.get(dataDirectory, fileName);
        
        if (Files.exists(file))
        {
            response.addHeader("Content-Disposition", "attachment; filename="+fileName);
            
            try
            {
                response.addHeader("Content-Length", Files.size(file) + "");
                response.addHeader("Content-Type", "application/octet-stream");
                 response.addHeader("Accept-Ranges", "bytes");
                Files.copy(file, response.getOutputStream());
                response.getOutputStream().flush();
            } 
            catch (IOException ex) {
              ex.printStackTrace();
              
            }
        }
    }
    
    
    @RequestMapping(value = {"", "/", "/index"}, method = RequestMethod.GET)
    public String index() {
        return "index";
    }
 
    @RequestMapping(value = {"/schedule-specialist-talk" }, method = RequestMethod.GET)
    public String scheduleSpecialistTalk() {
        return "schedule-specialist-talk";
    }
    
    @RequestMapping(value = {"/trusted-identity-verification" }, method = RequestMethod.GET)
    public String trustedIdentityVerification() {
        return "trusted-identity-verification";
    }
    
    @RequestMapping(value = {"/remote-identity-verification" }, method = RequestMethod.GET)
    public String remoteIdentityVerification() {
        return "remote-identity-verification";
    }
    
    @RequestMapping(value = {"/face-to-face-identity-verification" }, method = RequestMethod.GET)
    public String faceToFaceIdentityVerification() {
        return "face-to-face-identity-verification";
    }
    
    @RequestMapping(value = {"/use-cases" }, method = RequestMethod.GET)
    public String customerOverview() {
        return "use-cases";
    }
    
    @RequestMapping(value = {"/partnerships" }, method = RequestMethod.GET)
    public String partnerShips() {
        return "partnerships";
    }
   
    @RequestMapping(value = {"/rabobank" }, method = RequestMethod.GET)
    public String rabobank() {
        return "rabobank";
    }
    
    @RequestMapping(value = {"/uk-home-office-euss" }, method = RequestMethod.GET)
    public String ukHomeOfficeEuss() {
        return "uk-home-office-euss";
    }
    
    @RequestMapping(value = {"/aegon" }, method = RequestMethod.GET)
    public String aegon() {
        return "aegon";
    }
    
    @RequestMapping(value = {"/industries/financial-services" }, method = RequestMethod.GET)
    public String financialServices() {
        return "industries/financial-services";
    }
    
    @RequestMapping(value = {"/industries/travel" }, method = RequestMethod.GET)
    public String travel() {
        return "industries/travel";
    }
    
    @RequestMapping(value = {"/industries/trust-service-providers" }, method = RequestMethod.GET)
    public String trustServiceProviders() {
        return "industries/trust-service-providers";
    }
    
    @RequestMapping(value = {"/industries/hiring" }, method = RequestMethod.GET)
    public String hiring() {
        return "industries/hiring";
    }
    
    @RequestMapping(value = {"/industries/egovernment" }, method = RequestMethod.GET)
    public String eGovernment() {
        return "industries/egovernment";
    }
    
    @RequestMapping(value = {"/industries/gaming-gambling" }, method = RequestMethod.GET)
    public String gamingGambling() {
        return "industries/gaming-gambling";
    }
    
    @RequestMapping(value = {"/industries/police-border-control" }, method = RequestMethod.GET)
    public String policeBorderControl() {
        return "industries/police-border-control";
    }
    
    @RequestMapping(value = {"/resources/blog" }, method = RequestMethod.GET)
    public String blog() {
        return "resources/blog";
    }
    
    @RequestMapping(value = {"/resources/papers-reports" }, method = RequestMethod.GET)
    public String papersReports() {
        return "resources/papers-reports";
    }
    
    @RequestMapping(value = {"/resources/webinars" }, method = RequestMethod.GET)
    public String webinars() {
        return "resources/webinars";
    }
    
    @RequestMapping(value = {"/about/company" }, method = RequestMethod.GET)
    public String company() {
        return "about/company";
    }
    
    @RequestMapping(value = {"/about/history" }, method = RequestMethod.GET)
    public String history() {
        return "about/history";
    }
    
    @RequestMapping(value = {"/about/certifications" }, method = RequestMethod.GET)
    public String certifications() {
        return "about/certifications";
    }
    
    @RequestMapping(value = {"/about/awards" }, method = RequestMethod.GET)
    public String awards() {
        return "about/awards";
    }
    
    @RequestMapping(value = {"/about/events" }, method = RequestMethod.GET)
    public String events() {
        return "about/events";
    }
    
    @RequestMapping(value = {"/about/careers" }, method = RequestMethod.GET)
    public String careers() {
        return "about/careers";
    }
    
    @RequestMapping(value = {"/about/contact" }, method = RequestMethod.GET)
    public String contact() {
        return "about/contact";
    }
}