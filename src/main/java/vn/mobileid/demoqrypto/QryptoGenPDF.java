/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.mobileid.demoqrypto;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.html2pdf.resolver.font.DefaultFontProvider;
import com.itextpdf.io.font.FontProgramFactory;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.html2pdf.resolver.font.DefaultFontProvider;
import com.itextpdf.io.font.FontProgram;
import com.itextpdf.io.font.FontProgramFactory;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.text.DocumentException;
//import com.itextpdf.text.FontProvider;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import com.itextpdf.layout.font.FontProvider;


/**
 *
 * @author Mobile ID 22
 */
public class QryptoGenPDF {
    public static String readTemplateFile(String sPath) throws IOException {
        StringBuilder stringBuilder;
        try (BufferedReader reader = new BufferedReader(new FileReader(sPath))) {
            stringBuilder = new StringBuilder();
            String line;
            String ls = System.getProperty("line.separator");
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }   stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        }

        return stringBuilder.toString();
    }

   public static String createXMLReportControlFinal(String sFirstName, String sLastName, String sTanValue,
        String sDateOfBirth, String sAge, String sVillage, String sDistrict, String sProvince, String sMedicinalProduct,
        String sBatchNo, String sDateOfVaccination, String sSiteOfVaccination, String sCertificateIssuer, String sVaccineId,
        String sTotalDoses, String sDoseNumber)
            throws TransformerException, ParserConfigurationException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        // ROOT elements
        Document doc = docBuilder.newDocument();
        Element elmRoot = doc.createElement("KYC");
//        elmRoot.appendChild(doc.createTextNode(sFirstName));
        doc.appendChild(elmRoot);

        Element fromDateElm = doc.createElement("FirstName");
        fromDateElm.appendChild(doc.createTextNode(sFirstName));
        elmRoot.appendChild(fromDateElm);
        
        Element toLastName = doc.createElement("LastName");
        toLastName.appendChild(doc.createTextNode(sLastName));
        elmRoot.appendChild(toLastName);
        
        Element toTanValue = doc.createElement("TanValue");
        toTanValue.appendChild(doc.createTextNode(sTanValue));
        elmRoot.appendChild(toTanValue);
        Element toDateOfBirth = doc.createElement("DateOfBirth");
        toDateOfBirth.appendChild(doc.createTextNode(sDateOfBirth));
        elmRoot.appendChild(toDateOfBirth);
        
        Element toAge = doc.createElement("Age");
        toAge.appendChild(doc.createTextNode(sAge));
        elmRoot.appendChild(toAge);
        
        Element toVillage = doc.createElement("Village");
        toVillage.appendChild(doc.createTextNode(sVillage));
        elmRoot.appendChild(toVillage);
        
        Element toDistrict = doc.createElement("District");
        toDistrict.appendChild(doc.createTextNode(sDistrict));
        elmRoot.appendChild(toDistrict);
        
        Element toProvince = doc.createElement("Province");
        toProvince.appendChild(doc.createTextNode(sProvince));
        elmRoot.appendChild(toProvince);
        
        Element toMedicinalProduct = doc.createElement("MedicinalProduct");
        toMedicinalProduct.appendChild(doc.createTextNode(sMedicinalProduct));
        elmRoot.appendChild(toMedicinalProduct);
        
        Element toBatchNo = doc.createElement("BatchNo");
        toBatchNo.appendChild(doc.createTextNode(sBatchNo));
        elmRoot.appendChild(toBatchNo);
        
        Element toDateOfVaccination = doc.createElement("DateOfVaccination");
        toDateOfVaccination.appendChild(doc.createTextNode(sDateOfVaccination));
        elmRoot.appendChild(toDateOfVaccination);
        
        Element toSiteOfVaccination = doc.createElement("SiteOfVaccination");
        toSiteOfVaccination.appendChild(doc.createTextNode(sSiteOfVaccination));
        elmRoot.appendChild(toSiteOfVaccination);
        
        Element toCertificateIssuer = doc.createElement("CertificateIssuer");
        toCertificateIssuer.appendChild(doc.createTextNode(sCertificateIssuer));
        elmRoot.appendChild(toCertificateIssuer);
        
        
        Element toVaccineId = doc.createElement("VaccineId");
        toVaccineId.appendChild(doc.createTextNode(sVaccineId));
        elmRoot.appendChild(toVaccineId);
        
        Element toTotalDoses = doc.createElement("TotalDoses");
        toTotalDoses.appendChild(doc.createTextNode(sTotalDoses));
        elmRoot.appendChild(toTotalDoses);
        
        Element toDoseNumber = doc.createElement("DoseNumber");
        toDoseNumber.appendChild(doc.createTextNode(sDoseNumber));
        elmRoot.appendChild(toDoseNumber);
        
//        Element toqrCode = doc.createElement("qrCode");
//        toqrCode.appendChild(doc.createTextNode(qrCode));
//        elmRoot.appendChild(toqrCode);
        
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);

        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        transformer.transform(source, result);
        String xml = writer.toString();
        return xml;
    }
   
   public static String createXMLReportControlFinalOldVac(String sVaccineProphylaxis, String sDiseaseAgent, String sMedicinalProduct,
        String sAuthorisationHolderManufacturer, String sDoseNumber, String sTotalDoses, String sDateOfVaccination, String sIssuerCountry,
        String sCertificateIssuer, String sStandardisedFamilyName, String sStandardisedGivenName, String sDateOfBirth, String sUvci1, String sUvci2 )
            throws TransformerException, ParserConfigurationException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        // ROOT elements
        Document doc = docBuilder.newDocument();
        Element elmRoot = doc.createElement("KYC");
        doc.appendChild(elmRoot);

        Element fromDateElm = doc.createElement("VaccineProphylaxis");
        fromDateElm.appendChild(doc.createTextNode(sVaccineProphylaxis));
        elmRoot.appendChild(fromDateElm);
        
        Element toLastName = doc.createElement("DiseaseAgent");
        toLastName.appendChild(doc.createTextNode(sDiseaseAgent));
        elmRoot.appendChild(toLastName);
        
        Element toTanValue = doc.createElement("MedicinalProduct");
        toTanValue.appendChild(doc.createTextNode(sMedicinalProduct));
        elmRoot.appendChild(toTanValue);
        Element toDateOfBirth = doc.createElement("AuthorisationHolderManufacturer");
        toDateOfBirth.appendChild(doc.createTextNode(sAuthorisationHolderManufacturer));
        elmRoot.appendChild(toDateOfBirth);
        
        Element toAge = doc.createElement("DoseNumber");
        toAge.appendChild(doc.createTextNode(sDoseNumber));
        elmRoot.appendChild(toAge);
        
        Element toVillage = doc.createElement("TotalDoses");
        toVillage.appendChild(doc.createTextNode(sTotalDoses));
        elmRoot.appendChild(toVillage);
        
        Element toDistrict = doc.createElement("DateOfVaccination");
        toDistrict.appendChild(doc.createTextNode(sDateOfVaccination));
        elmRoot.appendChild(toDistrict);
        
        Element toProvince = doc.createElement("IssuerCountry");
        toProvince.appendChild(doc.createTextNode(sIssuerCountry));
        elmRoot.appendChild(toProvince);
        
        Element toMedicinalProduct = doc.createElement("CertificateIssuer");
        toMedicinalProduct.appendChild(doc.createTextNode(sCertificateIssuer));
        elmRoot.appendChild(toMedicinalProduct);
        
        Element toBatchNo = doc.createElement("StandardisedFamilyName");
        toBatchNo.appendChild(doc.createTextNode(sStandardisedFamilyName));
        elmRoot.appendChild(toBatchNo);
        
        Element toDateOfVaccination = doc.createElement("StandardisedGivenName");
        toDateOfVaccination.appendChild(doc.createTextNode(sStandardisedGivenName));
        elmRoot.appendChild(toDateOfVaccination);
        
        Element toSiteOfVaccination = doc.createElement("DateOfBirth");
        toSiteOfVaccination.appendChild(doc.createTextNode(sDateOfBirth));
        elmRoot.appendChild(toSiteOfVaccination);
        
        Element toUvci1 = doc.createElement("uvci1");
        toUvci1.appendChild(doc.createTextNode(sUvci1));
        elmRoot.appendChild(toUvci1);
        
        Element toUvci2 = doc.createElement("uvci2");
        toUvci2.appendChild(doc.createTextNode(sUvci2));
        elmRoot.appendChild(toUvci2);
        
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);

        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        transformer.transform(source, result);
        String xml = writer.toString();
        return xml;
    }
   
   public static String createXMLReportControlFinalOldTest(String sTypeOfTest, String sDiseaseAgent, String sTestName,
        String sTestManufacturer, String sDateTimeTestSample, String sTestResult, String sTestCentre, String sIssuerCountry,
        String sCertificateIssuer, String sStandardisedFamilyName, String sStandardisedGivenName, String sDateOfBirth, String sUvci1, String sUvci2 )
            throws TransformerException, ParserConfigurationException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        // ROOT elements
        Document doc = docBuilder.newDocument();
        Element elmRoot = doc.createElement("KYC");
        doc.appendChild(elmRoot);

        Element fromDateElm = doc.createElement("TypeOfTest");
        fromDateElm.appendChild(doc.createTextNode(sTypeOfTest));
        elmRoot.appendChild(fromDateElm);
        
        Element toLastName = doc.createElement("DiseaseAgent");
        toLastName.appendChild(doc.createTextNode(sDiseaseAgent));
        elmRoot.appendChild(toLastName);
        
        Element toTanValue = doc.createElement("TestName");
        toTanValue.appendChild(doc.createTextNode(sTestName));
        elmRoot.appendChild(toTanValue);
        
        Element toDateOfBirth = doc.createElement("TestManufacturer");
        toDateOfBirth.appendChild(doc.createTextNode(sTestManufacturer));
        elmRoot.appendChild(toDateOfBirth);
        
        Element toAge = doc.createElement("DateTimeTestSample");
        toAge.appendChild(doc.createTextNode(sDateTimeTestSample));
        elmRoot.appendChild(toAge);
        
        Element toVillage = doc.createElement("TestResult");
        toVillage.appendChild(doc.createTextNode(sTestResult));
        elmRoot.appendChild(toVillage);
        
        Element toDistrict = doc.createElement("TestCentre");
        toDistrict.appendChild(doc.createTextNode(sTestCentre));
        elmRoot.appendChild(toDistrict);
        
        Element toProvince = doc.createElement("IssuerCountry");
        toProvince.appendChild(doc.createTextNode(sIssuerCountry));
        elmRoot.appendChild(toProvince);
        
        Element toMedicinalProduct = doc.createElement("CertificateIssuer");
        toMedicinalProduct.appendChild(doc.createTextNode(sCertificateIssuer));
        elmRoot.appendChild(toMedicinalProduct);
        
        Element toBatchNo = doc.createElement("StandardisedFamilyName");
        toBatchNo.appendChild(doc.createTextNode(sStandardisedFamilyName));
        elmRoot.appendChild(toBatchNo);
        
        Element toDateOfVaccination = doc.createElement("StandardisedGivenName");
        toDateOfVaccination.appendChild(doc.createTextNode(sStandardisedGivenName));
        elmRoot.appendChild(toDateOfVaccination);
        
        Element toSiteOfVaccination = doc.createElement("DateOfBirth");
        toSiteOfVaccination.appendChild(doc.createTextNode(sDateOfBirth));
        elmRoot.appendChild(toSiteOfVaccination);
        
        Element toUvci1 = doc.createElement("uvci1");
        toUvci1.appendChild(doc.createTextNode(sUvci1));
        elmRoot.appendChild(toUvci1);
        
        Element toUvci2 = doc.createElement("uvci2");
        toUvci2.appendChild(doc.createTextNode(sUvci2));
        elmRoot.appendChild(toUvci2);
        
        
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);

        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        transformer.transform(source, result);
        String xml = writer.toString();
        return xml;
    }
   
   
    public static String createXMLReportControlFinalOldRecovery(String sDateOfPositive, String sDiseaseAgent, String sIssuerCountry,
        String sCertificateIssuer, String sValidFrom, String sValidTo , String sStandardisedFamilyName, String sStandardisedGivenName, String sDateOfBirth,
        String sUvci1, String sUvci2)
            throws TransformerException, ParserConfigurationException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        // ROOT elements
        Document doc = docBuilder.newDocument();
        Element elmRoot = doc.createElement("KYC");
        doc.appendChild(elmRoot);

        Element fromDateElm = doc.createElement("DateOfPositive");
        fromDateElm.appendChild(doc.createTextNode(sDateOfPositive));
        elmRoot.appendChild(fromDateElm);
        
        Element toLastName = doc.createElement("DiseaseAgent");
        toLastName.appendChild(doc.createTextNode(sDiseaseAgent));
        elmRoot.appendChild(toLastName);
        
        Element toProvince = doc.createElement("IssuerCountry");
        toProvince.appendChild(doc.createTextNode(sIssuerCountry));
        elmRoot.appendChild(toProvince);
        
        Element toMedicinalProduct = doc.createElement("CertificateIssuer");
        toMedicinalProduct.appendChild(doc.createTextNode(sCertificateIssuer));
        elmRoot.appendChild(toMedicinalProduct);
        
        Element toVillage = doc.createElement("ValidFrom");
        toVillage.appendChild(doc.createTextNode(sValidFrom));
        elmRoot.appendChild(toVillage);
        
        Element toDistrict = doc.createElement("ValidTo");
        toDistrict.appendChild(doc.createTextNode(sValidTo));
        elmRoot.appendChild(toDistrict);
        
        Element toBatchNo = doc.createElement("StandardisedFamilyName");
        toBatchNo.appendChild(doc.createTextNode(sStandardisedFamilyName));
        elmRoot.appendChild(toBatchNo);
        
        Element toDateOfVaccination = doc.createElement("StandardisedGivenName");
        toDateOfVaccination.appendChild(doc.createTextNode(sStandardisedGivenName));
        elmRoot.appendChild(toDateOfVaccination);
        
        Element toSiteOfVaccination = doc.createElement("DateOfBirth");
        toSiteOfVaccination.appendChild(doc.createTextNode(sDateOfBirth));
        elmRoot.appendChild(toSiteOfVaccination);
        
        Element toUvci1 = doc.createElement("uvci1");
        toUvci1.appendChild(doc.createTextNode(sUvci1));
        elmRoot.appendChild(toUvci1);
        
        Element toUvci2 = doc.createElement("uvci2");
        toUvci2.appendChild(doc.createTextNode(sUvci2));
        elmRoot.appendChild(toUvci2);
        
        
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);

        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        transformer.transform(source, result);
        String xml = writer.toString();
        return xml;
    }
   
   
   
   
   
   
    public static String createStringHtmlInString(String strXslt, String strData, int[] intResult) {
        String response = null;
        StringWriter writer = new StringWriter();
        try {
            InputStream inputXslt = new ByteArrayInputStream(strXslt.getBytes(Charset.forName("UTF-8")));
            InputStream inputData = new ByteArrayInputStream(strData.getBytes(Charset.forName("UTF-8")));
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer(
                    new javax.xml.transform.stream.StreamSource(inputXslt));
            transformer.transform(
                    new javax.xml.transform.stream.StreamSource(inputData),
                    new javax.xml.transform.stream.StreamResult(writer));
            String html = writer.toString();
            response = html;
            intResult[0] = 0;
        } catch (TransformerException ex) {
            intResult[0] = 1;
//            CommonFunction.LogExceptionServlet(log, "PrintFormFunction: " + ex.getMessage(), ex);
            System.out.println(ex.getMessage());
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                intResult[0] = 1;
//                CommonFunction.LogExceptionServlet(log, "PrintFormFunction: " + e.getMessage(), e);
                System.out.println(e.getMessage());
            }

        }
        return response;
    }

    public static void CreatePDFFromHTML(String strHTML, String sPathPDF, int[] resCode) {
        try {
//            System.out.println("A: " + strHTML);
            //String fileFont = "D:\\Project\\Hoa Don Dien Tu\\FPT HDDT\\CoreWs\\FPTEInvoiceData\\0312671405\\0306555016\\01GTKT0_004\\AC_12E/NotoNaskhArabic-Regular.ttf";
            com.itextpdf.text.Document document = new com.itextpdf.text.Document(com.itextpdf.text.PageSize.LETTER);
            com.itextpdf.text.pdf.PdfWriter pdfWriter = com.itextpdf.text.pdf.PdfWriter.getInstance(document, new FileOutputStream(sPathPDF));
            document.open();
            //String str = ViewStringFile(HTML);
            // CSS - access style css
            CSSResolver cssResolver = XMLWorkerHelper.getInstance().getDefaultCssResolver(true);
            // HTML
            //CssAppliers cssAppliers = new CssAppliersImpl(null);
            // Styles
//            CSSResolver cssResolver = new StyleAttrCSSResolver();
//            XMLWorkerFontProvider fontProvider = new XMLWorkerFontProvider(XMLWorkerFontProvider.DONTLOOKFORFONTS);
//            fontProvider.register("D:\\Project\\Hoa Don Dien Tu\\FPT HDDT\\CoreWs\\FPTEInvoiceData\\0312671405\\0306555016\\01GTKT0_004\\AC_12E/NotoNaskhArabic-Regular.ttf");
//            CssAppliers cssAppliers = new CssAppliersImpl(fontProvider);
            //
            HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
            htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
            //CSSResolver cssResolver = XMLWorkerHelper.getInstance().getDefaultCssResolver(true);
            // Pipelines
            PdfWriterPipeline pdf = new PdfWriterPipeline(document, pdfWriter);
            HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
            CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
            // XML Worker
            XMLWorker worker = new XMLWorker(css, true);
//            XMLParser p = new XMLParser(worker);
//            p.parse(new ByteArrayInputStream(strHTML.getBytes("UTF-8")));

            final Charset charset = Charset.forName("UTF-8");
            final XMLParser xmlParser = new XMLParser(true, worker, charset);
            xmlParser.parse(new ByteArrayInputStream(strHTML.getBytes("UTF-8")), charset);

//            File file = new File(strHTML);
//            byte[] data = FileUtils.readFileToByteArray(file);
//            p.parse(new ByteArrayInputStream(data));
//            p.parse(new ByteArrayInputStream(strHTML.getBytes("UTF-8")));
            document.close();
            resCode[0] = 0;
        } catch (DocumentException | IOException e) {
            resCode[0] = 1;
//            CommonFunction.LogExceptionServlet(log, "CreatePDFFromHTML: " + e.getMessage(), e);
            System.out.println(e.getMessage());
        }
    }
    
     public static final String[] FONTS = {
//            "/home/SONTP/NotoSans-Regular.ttf",
//            "/home/SONTP/NotoSansLao-Black.ttf",
//            "/home/SONTP/NotoSansLao-Bold.ttf",
//            "/home/SONTP/NotoSansLao-ExtraBold.ttf",
//            "/home/SONTP/NotoSansLao-ExtraLight.ttf",
//            "/home/SONTP/NotoSansLao-Light.ttf",
//            "/home/SONTP/NotoSansLao-Medium.ttf",
//            "/home/SONTP/NotoSansLao-Regular.ttf",
//            "/home/SONTP/NotoSansLao-SemiBold.ttf",
//            "/home/SONTP/NotoSansLao-Thin.ttf",
            
            
//            "/home/SONTP/poppins/Poppins-Medium.ttf",
//            "/home/SONTP/poppins/Poppins-MediumItalic.ttf",
//            "/home/SONTP/poppins/Poppins-Light.ttf",
//            "/home/SONTP/poppins/Poppins-LightItalic.ttf",
//            "/home/SONTP/poppins/Poppins-Regular.ttf",
//            "/home/SONTP/poppins/Poppins-Thin.ttf",
//            "/home/SONTP/poppins/Poppins-ThinItalic.ttf",
//            "/home/SONTP/poppins/Poppins-SemiBold.ttf",
//            "/home/SONTP/poppins/Poppins-SemiBoldItalic.ttf",
//            "/home/SONTP/poppins/Poppins-ExtraBoldItalic.ttf",
//            "/home/SONTP/poppins/Poppins-ExtraBold.ttf",
//            "/home/SONTP/poppins/Poppins-ExtraLight.ttf",
//            "/home/SONTP/poppins/Poppins-Italic.ttf",
//            "/home/SONTP/poppins/Poppins-ExtraLightItalic.ttf",
//            "/home/SONTP/poppins/Poppins-BlackItalic.ttf",
//            "/home/SONTP/poppins/Poppins-Black.ttf",
//            "/home/SONTP/poppins/Poppins-BoldItalic.ttf",
//            "/home/SONTP/poppins/Poppins-Bold.ttf",
    
            "/opt/wildfly/qrypto/deployments/NotoSans-Regular.ttf",
            "/opt/wildfly/qrypto/deployments/NotoSansLao-Black.ttf",
            "/opt/wildfly/qrypto/deployments/NotoSansLao-Bold.ttf",
            "/opt/wildfly/qrypto/deployments/NotoSansLao-ExtraBold.ttf",
            "/opt/wildfly/qrypto/deployments/NotoSansLao-ExtraLight.ttf",
            "/opt/wildfly/qrypto/deployments/NotoSansLao-Light.ttf",
            "/opt/wildfly/qrypto/deployments/NotoSansLao-Medium.ttf",
            "/opt/wildfly/qrypto/deployments/NotoSansLao-Regular.ttf",
            "/opt/wildfly/qrypto/deployments/NotoSansLao-SemiBold.ttf",
            "/opt/wildfly/qrypto/deployments/NotoSansLao-Thin.ttf"
     };
     
     public static void createPdf(String src, String[] fonts, String dest) throws IOException {
        ConverterProperties properties = new ConverterProperties();
        FontProvider fontProvider = new DefaultFontProvider(false, false, false);
        for (String font : fonts) {
            FontProgram fontProgram = FontProgramFactory.createFont(font);
            fontProvider.addFont(fontProgram);
        }
        properties.setFontProvider(fontProvider);
        HtmlConverter.convertToPdf(new File(src), new File(dest), properties);
    }
    
    public static void SaveFile(String urlFile, byte[] data, int[] resCode) {
        try {
            File files = new File(urlFile);
            if (!files.exists()) {
                if (files.createNewFile()) {
                    files.setReadable(true);
                    files.setWritable(true);
                    FileOutputStream fos = new FileOutputStream(files, false);
                    BufferedOutputStream outputStream = new BufferedOutputStream(fos);
                    outputStream.write(data);
                    outputStream.close();
                    fos.close();
                    resCode[0] = 0;
                } else {
                    resCode[0] = 1;
                }
            } else {
                files.setReadable(true);
                files.setWritable(true);
                FileOutputStream fos = new FileOutputStream(files, false);
                BufferedOutputStream outputStream = new BufferedOutputStream(fos);
                outputStream.write(data);
                outputStream.close();
                fos.close();
            }

        } catch (Exception ex) {
            resCode[0] = 2;
            System.out.println("EX: " + ex.getMessage());
        }

    }
    
}
