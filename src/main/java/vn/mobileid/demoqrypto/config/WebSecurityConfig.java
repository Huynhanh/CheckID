/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.mobileid.demoqrypto.config;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
//import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
/**
 *
 * @author SonThai
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
@EnableAsync
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
//        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry = http.antMatcher("/**").authorizeRequests();
//        registry.and().headers().xssProtection();
//        registry.and().headers().frameOptions().disable();
//        registry.and().headers().httpStrictTransportSecurity();
//        registry.and().cors().disable();
////        registry.and().csrf();
//        registry.and().csrf()
//                .csrfTokenRepository(new HttpSessionCsrfTokenRepository());
//        registry.anyRequest().permitAll();
//        registry.anyRequest().permitAll();
//        http.csrf().disable();
//        http.cors().disable();
//        http.httpBasic().disable();
    }
}
