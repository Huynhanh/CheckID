/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.mobileid.demoqrypto.config;

/**
 *
 * @author SonThai
 */
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.support.RequestContextUtils;

public class UrlLocaleInterceptor extends HandlerInterceptorAdapter {

   @Override
   public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
           throws Exception {

       LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

       if (localeResolver == null) {
           throw new IllegalStateException("No LocaleResolver found: not in a DispatcherServlet request?");
       }

       // Lấy ra thông tin Locale từ LocaleResolver
       Locale locale = localeResolver.resolveLocale(request);

       localeResolver.setLocale(request, response, locale);

       return true;
   }

}
